﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class numberOFActivatedCards : MonoBehaviour {

	private GameObject saveManager;

	public int totalActivatedCards;
	public int totalCards;

	private Text numOfActivated;


	// Use this for initialization
	void Start () {
		
		numOfActivated = gameObject.GetComponent<Text>();
		
	}


	public void checkNumberOFCards(){




		saveManager = GameObject.Find("Save Manager");
		initalizeSaveSystem numOfActivatedCardsScript = saveManager.GetComponent<initalizeSaveSystem>();
		numOfActivatedCardsScript.checkActivatedCards ();
		totalActivatedCards = numOfActivatedCardsScript.totalActivatedCards;

		totalCards = numOfActivatedCardsScript.totalNumberOfCards;
		
		numOfActivated.text =  totalActivatedCards +"/"+totalCards;
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class pageThumbnailStreamLoad : MonoBehaviour {

	private int PageNumber;
	private float percentageLoaded = 0;

	// Use this for initialization
	void Start () {
		PageNumber = GetComponent<buttonClickControl> ().pageNum;
	}


	// Update is called once per frame
	void Update () {

		//Scene to load adding 1 to the PageNumber to match the scene index
		
		if (PageNumber != null) {
			if (Application.GetStreamProgressForLevel (PageNumber+3) == 2){

		} else {

				percentageLoaded = Application.GetStreamProgressForLevel(PageNumber+1) * 100;

		
			}

		} 

		if (percentageLoaded == 100) {
			
			GetComponent<Button>().interactable = true;
			gameObject.SetActive(true);
		} else {
			GetComponent<Button>().interactable = false;
			
		}

	
	}

}

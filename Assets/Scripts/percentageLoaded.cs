﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class percentageLoaded : MonoBehaviour {

	public GameObject mainComicThumb;
	public GameObject downloadCircleButton;
	public GameObject circleGraphic;

	private Text percentage;
	private downloadComicServer downloadComicServer;


	void Start(){

		percentage = GetComponent<Text> ();

		downloadComicServer = mainComicThumb.GetComponent<downloadComicServer> ();


		
	}

	void Update(){

		if (downloadComicServer.downloadComplete == false) {
		
			float percentageLoaded = downloadComicServer.percentDownloaded;
			percentageLoaded = Mathf.Round (percentageLoaded * 100f);

			if (percentageLoaded == 0){

				downloadCircleButton.SetActive(true);
				circleGraphic.SetActive(false);
			} else if (percentageLoaded == 100){
				downloadCircleButton.SetActive(false);
				circleGraphic.SetActive(false);

			} else {
				percentage.text = percentageLoaded.ToString () + "%";
				downloadCircleButton.SetActive(false);
				circleGraphic.SetActive(true);
			}
			

		} else {
		
			percentage.text = "";
			downloadCircleButton.SetActive(false);
			circleGraphic.SetActive(false);
			gameObject.SetActive(false);
		}


	}
}

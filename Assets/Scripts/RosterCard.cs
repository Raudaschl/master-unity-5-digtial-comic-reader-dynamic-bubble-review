﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RosterCard : MonoBehaviour 
{
	GameObject SubMenu;

	//Card int which defines type of card to be used - this is pulled from the hotspot script
	public int myCardInt;

	//Assign aniamtor for card
	Animator anim;

	//Card Movement Trigger
	int CardMoveEnd = 0;

	//Get target tranform that card moves too on compeltetion 
	public Transform CardTarget;
	public float CardSpeed;
	

	//Textures for different cards

	public Texture Card01;
	public Texture Card02;
	public Texture Card03;
	public Texture Card04;
	public Texture Card05;
	public Texture Card06;
	public Texture Card07;
	public Texture Card08;
	public Texture Card09;

	//Navobjects
	private GameObject[] hotspotInteractives;
	private GameObject[] RosterThumbs;
	private GameObject collectAlert;
	private Transform hotSpotPosition;


	//Audio

	private AudioSource audioSource;
	public AudioClip closeSound;


	void Awake(){
		audioSource = GetComponent<AudioSource> ();
		audioSource.clip = closeSound;

	}

	// Use this for initialization
	void Start () 
	{

		SubMenu = GameObject.Find ("SubMenu");

//		CardTarget = GameObject.Find ("Card Target").transform;

		collectAlert = (GameObject)Resources.Load("collectAlert", typeof(GameObject));



		//Set up arrays of objects based on TAG

//		
//		hotspotInteractives = GameObject.FindGameObjectsWithTag ("HotSpot");
	
	}
	

//	void Update() 
//	{
//		//If Card is exit command is hit then move card to a caertain target then end this function
//		if (CardMoveEnd == 1)
//		{
//			float step = CardSpeed * Time.deltaTime;
//			transform.position = Vector3.MoveTowards (transform.position, CardTarget.position, step);
//				//End card movement
//				if (transform.position == CardTarget.position) 
//				{
//					CardMoveEnd = 0;
//				}
//		}
//
//	}

	//Different Card Graphics
	public void CardStartTrigger()
	{
		switch (myCardInt)
		{
		case 0:
		{
			
			GetComponent<Renderer>().material.mainTexture = Card01;
			
			//Activate IEnumerator BeginCardAnimation
			StartCoroutine("BeginCardAnimation", 0.1F);
			break;
		}
			
		case 1:
		{
			GetComponent<Renderer>().material.mainTexture = Card02;
			//Activate IEnumerator BeginCardAnimation
			StartCoroutine("BeginCardAnimation", 0.1F);
			break;
		}
			
		case 2:
		{
			GetComponent<Renderer>().material.mainTexture = Card03;
			//Activate IEnumerator BeginCardAnimation
			StartCoroutine("BeginCardAnimation", 0.1F);
			break;
		}

		case 3:
		{
			GetComponent<Renderer>().material.mainTexture = Card04;
			//Activate IEnumerator BeginCardAnimation
			StartCoroutine("BeginCardAnimation", 0.1F);
			break;
		}

		case 4:
		{
			GetComponent<Renderer>().material.mainTexture = Card05;
			//Activate IEnumerator BeginCardAnimation
			StartCoroutine("BeginCardAnimation", 0.1F);
			break;
		}

		case 5:
		{
			GetComponent<Renderer>().material.mainTexture = Card06;
			//Activate IEnumerator BeginCardAnimation
			StartCoroutine("BeginCardAnimation", 0.1F);
			break;
		}

		case 6:
		{
			GetComponent<Renderer>().material.mainTexture = Card07;
			//Activate IEnumerator BeginCardAnimation
			StartCoroutine("BeginCardAnimation", 0.1F);
			break;
		}

		case 7:
		{
			GetComponent<Renderer>().material.mainTexture = Card08;
			//Activate IEnumerator BeginCardAnimation
			StartCoroutine("BeginCardAnimation", 0.1F);
			break;
		}

		case 8:
		{
			GetComponent<Renderer>().material.mainTexture = Card09;
			//Activate IEnumerator BeginCardAnimation
			StartCoroutine("BeginCardAnimation", 0.1F);
			break;
		}

			
			
		}

	}


	void OnMouseDown()
	{
		//Activate end card behaviour
		EndCard ();
	}
	
	void EndCard()
	{
	

		audioSource.Play ();

		//Start end card movement and start dissappear animation
		CardMoveEnd = 1;
		anim.SetBool("RostCard_Dissappear", true);


		StartCoroutine("ActivateSceneNav", 0.1F);

	}

	public void EndCardFromMenu()
	{

		audioSource.Play ();
		//Start end card movement and start dissappear animation
		CardMoveEnd = 1;
		anim.SetBool("RostCard_Dissappear", true);

		
	}

	IEnumerator BeginCardAnimation(float waitTime)
	{
		//Aniamtion Trigger Start
		anim = GetComponent<Animator>();
		anim.SetBool("RostCard_Begin", true);
		anim.SetBool("RostCard_Dissappear", false);

		yield return new WaitForSeconds(waitTime);
		anim.SetBool("RostCard_Begin", false);

	}
	
	public void destroy() { 

		Destroy (gameObject);

    	SubMenu.GetComponent<SlideOut> ().Activate ();
		
	}

	//Activate scene navigation
	IEnumerator ActivateSceneNav(float waitTime2)
	{


		hotspotInteractives = GameObject.FindGameObjectsWithTag ("HotSpot");
		RosterThumbs = GameObject.FindGameObjectsWithTag ("RosterThumbs");


		//Turn back on raycast layer for nav

		yield return new WaitForSeconds(waitTime2);

		//Card Collected graphic power up and save to array that card has been collected

//		Debug.Log(myCardInt+1);
		if (ES2.Exists ("medikdizComic.txt?tag=Card"+(myCardInt+1))) 
		{
//			Debug.Log(ES2.Load<int>("medikdizComic.txt?tag=Card"+(myCardInt+1)));
			if (ES2.Load<int>("medikdizComic.txt?tag=Card"+(myCardInt+1)) == 0) 
			{
			hotSpotPosition = gameObject.transform;
				Instantiate (Resources.Load("collectAlert"), new Vector3(0.01f, -0.1f, -160), hotSpotPosition.rotation);

	
		//Tell save system that a roster card has been unlocked
				if (gameObject.CompareTag("RosterCard")) 
				{ 
					//NOTE always add 1 to enum int when saving so numbers match gameObject card number names e.g. Card1
					ES2.Save (1, "medikdizComic.txt?tag=Card" + (myCardInt + 1));
					
					GameObject RosterThumbCard = GameObject.Find ("Card" + (myCardInt + 1));
					
					RosterThumbCard.GetComponent<rosterThumbActivateCheck> ().rosterCardThumbActivateCheck ();
					
				}


			}
		}



		//Turn back on raycast layer for nav
//		foreach (GameObject RosterThumb in RosterThumbs) {
//			
////			RosterThumb.GetComponent<Button>().enabled = true;
//		}

		if (hotspotInteractives != null) {

			for (int index = 0; index < hotspotInteractives.Length; index++)
			{
				
				var hotspotInteractive = hotspotInteractives[index];
				hotspotInteractive.layer = LayerMask.NameToLayer ("Default");
				
			}
		}


	}
}
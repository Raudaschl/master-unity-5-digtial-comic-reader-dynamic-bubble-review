﻿using UnityEngine;
using System.Collections;

public enum HotSpotCard{
	Card1, Card2, Card3, Card4, Card5, Card6, Card7, Card8, Card9
}

public class hotSpotInstantiate : MonoBehaviour {

	//Card Selection
	public HotSpotCard HotSpotCardSelect;
	
	private GameObject HotSpotPrefab;
	private GameObject HotSpotPrefabFound;
	private Transform hotSpotPosition;


	void Start(){
		HotSpotPrefab = (GameObject)Resources.Load("HotSpotPrefab", typeof(GameObject));
		hotSpotPosition = gameObject.transform;


		//Temp

//
//		StartCoroutine("HotSpotCreate", 0.5f);

	}

	// Use this for initialization
	public void HotSpotCreate() {


		if (GameObject.Find ("HotSpotPrefab(Clone)") == null) {
	
			Instantiate (HotSpotPrefab, new Vector3(hotSpotPosition.position.x, hotSpotPosition.position.y, -150), hotSpotPosition.rotation);

			HotSpotPrefabFound = GameObject.Find ("HotSpotPrefab(Clone)");
			HotSpotPrefabFound.GetComponent<hotSpot>().myCardInt = (int)HotSpotCardSelect;


				}


	}
	

}

using UnityEngine;
using System.Collections;

public class controller : loading_prefab {
	
	string playing = null;
	
	private float cameraZoom;
	float aniLength = 0;
	
	public int pageNum = 0;
	public int portraitTrigger;
	public AssetBundle bundle;
	
	public loading_prefab parent;
	
	public pageLoadMode loader;
	
	
	
	void Start () {
		
		

		
		
		playing = GetComponent<Animation>().clip.name;
		Length ();

		if (Direction == 1) {
			
			GetComponent<Animation>()[playing].time = aniLength;
			Backward ();
		}
		
		//Load Camera component if none exists (this is mainly for the individual pages)
		
		if (GameObject.Find ("Camera_Nav") == null) {
			
			//Camera and Navigation Prefabs
			
			GameObject camera = (GameObject)Instantiate(Resources.Load("Camera_Nav"));
			
			camera.name = camera.name.Replace("(Clone)", "");
			
			//Global Audio
			GameObject LoaderFramework = (GameObject)Instantiate(Resources.Load("LoaderFramework"));
			
			LoaderFramework.name = LoaderFramework.name.Replace("Framework(Clone)", "");
			
			
			
		}
		
		//Find loader object
		
		parent = GameObject.Find ("Loader").GetComponent<loading_prefab> ();
		loader = parent.loadType;

		if (bundle == null) {
			bundle = parent.bundle;

		}
	

	}
	
	
	// Update is called once per frame
	void Update () {




		//Controls
		if (Input.GetKeyDown ("left")) {
			Backward();
		}
		
		if (Input.GetKeyDown ("right")) {
			Forward();
		}
		
		//End of frame trigger
		if (GetComponent<Animation>()[playing].normalizedTime >= 0.85F && Direction == 2) {
			audioFadeOut();
		}

	
			//End of frame audio fade out except for page 30 which goes to main menu
			if (GetComponent<Animation> () [playing].normalizedTime >= 0.99F && Direction == 2) {
				nextPage ();
			}
	

		
		
		//Control if animation is player
		
		foreach (AnimationState state in GetComponent<Animation>()) {
			if (state.speed == 1.0f || state.speed <= -1.0f) {
				GameObject go = GameObject.Find ("CanvasNav");
				go.GetComponent<touchControlCenter> ().moving ();
			} 
		}
		
		
		
	}
	
	//	void FixedUpdate() {
	//		
	//		cameraZoom = Camera.main.orthographicSize;
	//		
	//		// Change scene camera size based trigger
	//		
	//		if ( portraitTrigger == 1 && cameraZoom <= 6.71f ) {
	//			portraitTriggerZoomOut();
	//		}
	//		
	//		// Change scene camera size based trigger
	//		
	//		if ( portraitTrigger == 0 && cameraZoom > 5f ) {
	//			portraitTriggerZoomIn();
	//		}
	//		
	//		
	//	}
	
	
	
	public void Stopper() {
		//Stopping Script
		foreach (AnimationState state in GetComponent<Animation>()) {
			state.speed = 0F;
		loading_prefab: Direction = 0;
		}
		
		GameObject go = GameObject.Find ("CanvasNav");
		go.GetComponent<touchControlCenter> ().NotMoving ();
		
		
		
	}
	
	
	//Start Frame event
	void frameStart() {
		
		if (Direction == 1) {
			lastPage();
		}
	}
	
	
	//If user tries going backwards on first frame
	void frameBounce() {
		
		
		if (Direction == 1) {
			Forward();
		}
	}
	
	
	
	
	
	public void Backward() {
		foreach (AnimationState state in GetComponent<Animation>()) {
			state.speed = -3.0F;
		loading_prefab: Direction = 1;
			
			if (GameObject.Find("HotSpotPrefab(Clone)") != null )
			{
				GameObject Hot = GameObject.Find("HotSpotPrefab(Clone)");
				Hot.GetComponent<hotSpot>().fadeOutani();
			}
		}
	}
	
	public void Forward() {
		foreach (AnimationState state in GetComponent<Animation>()) {
			state.speed = 1.0F;
		loading_prefab: Direction = 2;
			
			//Activate hot spots only moving forward
			if (GameObject.Find("HotSpotPrefab(Clone)") != null )
			{
				GameObject Hot = GameObject.Find("HotSpotPrefab(Clone)");
				Hot.GetComponent<hotSpot>().fadeOutani();
			}
			
		}
	}
	
	//Page Transition Audio Fadeout
	void audioFadeOut() {
		GameObject fadingAudio = GameObject.Find ("Page");
		fadingAudio.GetComponent<audioPlay> ().background1_fadeout();
		fadingAudio.GetComponent<audioPlay> ().background2_fadeout();
		fadingAudio.GetComponent<audioPlay> ().background3_fadeout();
	}
	
	
	//Page Transition next page
	void nextPage() {

		if (pageNum != loading_prefab.pageCount) {
			pageChange (1);
		} else {
			GameObject FadeInOut = GameObject.FindWithTag ("FadeInOut");
			
			
			FadeInOut.GetComponent<FadeController> ().FadeOutMainMenu ();
		}


		
		
	}
	
	void pageChange(int pageValue){
		
		
		
		GameObject[] pageElements = GameObject.FindGameObjectsWithTag("Page");
		
		foreach (GameObject pageElement in pageElements) {
			Destroy (pageElement);
			
		}
		
		
		
		
		Destroy (GameObject.FindWithTag("HotSpot"));
		
		if (loader == pageLoadMode.Manual) {
			
			page = (GameObject)Instantiate (Resources.Load ("page" + (pageNum + pageValue)));
			pageNameClean ();
			
		} else if (loader == pageLoadMode.Server) {
			
	
			page = (GameObject)Instantiate(bundle.LoadAsset ("page"+ (pageNum + pageValue)));
			pageNameClean ();
			
			
		} else if (loader == pageLoadMode.WebPlayer || loader == null) {
			
			Application.LoadLevelAdditive ("page" + (pageNum + pageValue));
		} else {
			
			Debug.Log ("Can't find any scene");
		}
		
		
		
		
		Resources.UnloadUnusedAssets();


		
	}
	
	//Page Transition previous page
	void lastPage() {
		
		
		
		pageChange (-1);
		
		
	}
	
	//Page Transition to a page number elected by the thumb menu and called by the buttonClickControl script to pageNum
	public void loadPage() {
		
		
		GameObject[] pageElements = GameObject.FindGameObjectsWithTag("Page");
		
		foreach (GameObject pageElement in pageElements) {
			Destroy (pageElement);
			
		}
		
		
		Destroy (GameObject.FindWithTag("Audio"));
		Destroy (GameObject.FindWithTag("HotSpot"));


		if (loader == pageLoadMode.Manual) {
			
			page = (GameObject)Instantiate (Resources.Load ("page"+(pageNum)));
			pageNameClean ();
			
		} else if (loader == pageLoadMode.Server) {
			
			
			page = (GameObject)Instantiate(bundle.LoadAsset ("page"+(pageNum)));
			pageNameClean ();
			
			
		} else if (loader == pageLoadMode.WebPlayer || loader == null) {

			Application.LoadLevelAdditive("page"+(pageNum));
			Resources.UnloadUnusedAssets();
		} else {
			
			Debug.Log ("Can't find any scene");
		}

		
		//		GameObject comicPrefab = (GameObject)(Resources.Load("scoliosis"));
		//		
		//		GameObject pageFind = comicPrefab.transform.Find("page" + (pageNum)).gameObject;
		//		
		//		GameObject page = (GameObject)Instantiate(pageFind);
		
		//		GameObject page = (GameObject)
		//		Instantiate(Resources.Load("page"+ (pageNum)));
		
//		Application.LoadLevelAdditive("page"+(pageNum));
//		Resources.UnloadUnusedAssets();
		
		
		//		page.name = page.name.Replace("(Clone)", "");
		
		if (GameObject.Find("HotSpotPrefab(Clone)") != null )
		{
			GameObject Hot = GameObject.Find("HotSpotPrefab(Clone)");
			Hot.GetComponent<hotSpot>().fadeOutani();
		}
		
		Direction = 2;
		
	}
	
	//Length of Clip
	void Length() {
		aniLength = GetComponent<Animation>()[playing].length;
		
	}
	
	//Generate HotSpot
	public void HotSpotCreation(){
		if (Direction == 2) {
			GameObject hot = GameObject.Find ("Hot Spot Placement");
			hot.GetComponent<hotSpotInstantiate> ().HotSpotCreate ();
		}
	}
	
	//Portrait Trigger Turn on
	public void portraitTriggerOn() {
		
		//Trigger Portrait mode
		if (Input.deviceOrientation == DeviceOrientation.Portrait || Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown || Input.GetKeyDown ("p")) {
			portraitTrigger = 1;
			Debug.Log ("on");
			//			GameObject navOn = GameObject.Find ("Main Camera");
			//			navOn.GetComponent<orientation> ().UsePortraitLayoutZoomOut ();
			
		} else if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft || Input.deviceOrientation == DeviceOrientation.LandscapeRight || Input.GetKeyDown ("l")) {
			portraitTrigger = 0;
			Debug.Log ("off");
			
		}
		
		
		
		
	}
	
	public void portraitTriggerOff() {
		
		//Trigger Portrait mode
		portraitTrigger = 0;
		//		Debug.Log ("off Portrait mode");
		GameObject navOn = GameObject.Find ("Main Camera");
		navOn.GetComponent<orientation> ().UsePortraitLayout ();
	}
	
	
	//Portrait Trigger Camera Zoom Out
	void portraitTriggerZoomOut() {
		Camera.main.orthographicSize += 16f * Time.deltaTime;
		
	}
	
	//Landscape Trigger Camera Zoom Out
	void portraitTriggerZoomIn() {
		Camera.main.orthographicSize -= 16f * Time.deltaTime;
		
	}
	
	//What Page is this?
	void WhatPage() {
		//		GameObject thePlayer = GameObject.Find("Page");
		//		controller playerScript = thePlayer.GetComponent<controller>();
		//Debug.Log (playerScript.pageNum);
	}
	
	
	
	
}
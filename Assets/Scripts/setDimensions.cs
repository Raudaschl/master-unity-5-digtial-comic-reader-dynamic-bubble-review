﻿using UnityEngine;
using System.Collections;
using TMPro;

//[ExecuteInEditMode]
public class setDimensions : MonoBehaviour {

	public TextMeshPro text;

	private float widthScale;
	private float heightScale;


	void Awake(){
		float textWidth = text.GetComponent<TextContainer> ().width;
		float bubbleWidth = gameObject.transform.localScale.x;

		float textHeight = text.GetComponent<TextContainer> ().height;
		float bubbleHeight = gameObject.transform.localScale.y;
		
		
		widthScale = textWidth/bubbleWidth;
		heightScale = textHeight/bubbleHeight;
	
	}

	
	// Update is called once per frame
	void Update () {
		float width = text.GetComponent<TextContainer> ().width/widthScale;
		float height = text.GetComponent<TextContainer> ().height/heightScale;


		gameObject.transform.localScale = new Vector2(width, height); 

		Debug.Log (text.GetComponent<TextMeshPro>());
	}
}

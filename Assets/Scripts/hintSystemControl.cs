﻿using UnityEngine;
using System.Collections;

public class hintSystemControl : MonoBehaviour {

	protected Animator animator;

	public GameObject HintSystem;

	public void Pressed() {
		GameObject go = GameObject.Find("Page");
		go.GetComponent<controller>().Forward();


		animator = HintSystem.GetComponent<Animator> ();

		animator.SetBool ("hint_end", true);
	}
}

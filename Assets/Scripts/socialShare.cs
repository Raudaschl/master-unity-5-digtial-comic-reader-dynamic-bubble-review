﻿using UnityEngine;
using System.Collections;

public class socialShare : MonoBehaviour {

	private GameObject shareSubMenuObject;
	public Texture2D textureForPost;

	// Use this for initialization
	void Start () {
		 shareSubMenuObject = GameObject.Find ("Share SubMenu");
	}


	public void activatePostTwitterScreenshot(){
		StartCoroutine(PostTwitterScreenshot());
	}

	public void activatePostFBScreenshot(){
		StartCoroutine(PostFBScreenshot());
	}

	public void activateSendMailWithImage(){
		sendMailWithImage();
	}

	public void activatePostScreenshot(){
		StartCoroutine(PostScreenshot());
	}
	
	private IEnumerator PostTwitterScreenshot() {
		shareSubMenuObject.transform.localScale = new Vector3(0, 0, 0);
		
		yield return new WaitForEndOfFrame();
		// Create a texture the size of the screen, RGB24 format
		int width = Screen.width;
		int height = Screen.height;
		Texture2D tex = new Texture2D( width, height, TextureFormat.RGB24, false );
		// Read screen contents into the texture
		tex.ReadPixels( new Rect(0, 0, width, height), 0, 0 );
		tex.Apply();
		
//		UM_ShareUtility.TwitterShare("Medikidz Explain Multiple Sclerosis. Download today! http://www.medikidz.com/msau/ #Medikidz", tex);
		
		
		Destroy(tex);
		shareSubMenuObject.transform.localScale = new Vector3(1, 1, 1);
		
	}
	
	private IEnumerator PostFBScreenshot() {

		shareSubMenuObject.transform.localScale = new Vector3(0, 0, 0);
		

		yield return new WaitForEndOfFrame();
		// Create a texture the size of the screen, RGB24 format
		int width = Screen.width;
		int height = Screen.height;
		Texture2D tex = new Texture2D( width, height, TextureFormat.RGB24, false );
		// Read screen contents into the texture
		tex.ReadPixels( new Rect(0, 0, width, height), 0, 0 );
		tex.Apply();
		
		
//		UM_ShareUtility.FacebookShare("Medikidz Explain Multiple Sclerosis. Download today! http://www.medikidz.com/msau/", tex);
		
		Destroy(tex);

		shareSubMenuObject.transform.localScale = new Vector3(1, 1, 1);
		
	}

	 private void sendMailWithImage() {

//		UM_ShareUtility.SendMail ("I've been reading Medikidz Explain UCD!", "Medikidz Explain Multiple Sclerosis. Download today! http://www.medikidz.com/msau/", "", textureForPost);

	}

	private IEnumerator PostScreenshot() {
		shareSubMenuObject.transform.localScale = new Vector3(0, 0, 0);
		
		yield return new WaitForEndOfFrame();
		// Create a texture the size of the screen, RGB24 format
		int width = Screen.width;
		int height = Screen.height;
		Texture2D tex = new Texture2D( width, height, TextureFormat.RGB24, false );
		// Read screen contents into the texture
		tex.ReadPixels( new Rect(0, 0, width, height), 0, 0 );
		tex.Apply();
		
//		UM_ShareUtility.ShareMedia("Title", "Some text to share", tex);
		
		Destroy(tex);

		shareSubMenuObject.transform.localScale = new Vector3(1, 1, 1);
	}
}

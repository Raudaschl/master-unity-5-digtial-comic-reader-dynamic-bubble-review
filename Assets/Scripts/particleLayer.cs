﻿using UnityEngine;
using System.Collections;

public class particleLayer : MonoBehaviour {

	public int LayerNumber;

	void Start ()
	{
		// Set the sorting layer of the particle system.

		GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingOrder = LayerNumber;
	}
}

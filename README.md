# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
Introduction

The medikidz digital comic framework was designed to be modular in nature to enable maximum flexibility with the type of content it would be handling.

Though originally designed for comics, the framework is flexible enough to handle any unity created scenes in the landscape format and utilises only one 3rd party plugin for handling data storage.

As a warning, this guide does not set out to teach you about how unity 3d works, and presumes you have a basic understanding of unity 3d going in. If you are looking for a crash course introduction, I high recommend watching the unity introduction video series on their website (that’s how I got started):

http://unity3d.com/learn/tutorials/modules

Ok, lets get stuck in!


## Overall Layout ##

Overall Application Layout

In essence the framework is split into two main parts: the comic framework scene and the page itself.
The comic framework is like a wrapper which has all the main navigation, menus, data storage and interactive logic which can load and manage pages from the comic.
The page is a single scene in unity which contains all the assets from a page in the comic inclusive of artwork and audio. All animation, specialised interaction, audio integration happens within this page and can act independent of the comic framework scene. The only exception to this is if there is functionality/logic which requires data storage or has an impact on the rest of the comic e.g. audio playing between comic pages or remembering if a specialised input had occurred before.

So overall two main parts – the main comic framework scene and the individual page scenes – simple!

NOTE – All the additional elements in the main comic framework such as the menu’s are essentially instantiated as prefabs which in turn are generated from scenes. We will go into this in more detail later on, but you should not need to adjust these in any significant way.

## Comic Framework ##

### Comic Framework Anatomy ###

The comic framework is really quite simple when you look at it. There are not many objects apart from a loader, save manager, camera and navigation and card target. This is because the loader will instantiate the majority of required objects such as the submenu, credits menu, tutorial, medikidz collection, exit menu and share menu from prefabs. This helps ensure these elements are kept separate from the core functionality of the framework and can be swapped out easily if required.
In the following section I will go over all the objects which work within this core framework and how they function on a basic level.



### Camera and navigation ###

The camera and navigation is instantiated from a prefab by the loader object in the main framework. The prefab is generated from the controls_camera scene located in the Scenes folder. This object contains a static camera in the orthographic perspective (fancy way of saying that the camera removes a sense of the depth of field so things look flatter even if they are 3d) as well as logic which divides the screen up into thirds allowing navigation by tapping the left or right sides of the screen, or tapping the center to bring up the sub-menu.

It also contains a script which detects horizontal finger swipes; the sensitivity of which can be adjusted in the inspector.
This object also handles most of the garbage handling in the comic making sure unused data is removed every time a new page/scene is loaded. Not having this enabled will ensure our app will crash about 10 pages in on a mobile device.



### Loader ###

The loader object handles the instantiation of all prefabs which makeup the various menu items and ensures the first correct page is dynamically added into the framework. in the inspector the user is able to select the first page to load for debugging purposes.

## loader ##

### Global Audio ###

This object is nested within the loader object. To understand the global audio object you have to understand how audio generally works in this app. There are essentially two types of audio –

Local audio which is audio localised to a single page/scene in the comic all of which will stop playing should the user change the page/scene
Global audio which is an audio player that exists across all scenes loaded into the framework. Global audio would be an advantage for example if you wanted a single piece of music to keep playing as you go between pages without stopping when the scene is changed.
This object allows you to define different audio clips which will be assigned a specific identifier that can be then called using an event trigger in an animation timeline. We will go into more detail on how to assign audio to our comic pages later, but right now you just need to be aware that your global audio files are defined here and that to hear global audio files in action you must use the comic framework scene.
Clicking on the global audio object and then looking in the inspector reveals a range of sliders numbers 1-10; adjusting these sliders changes the audio fadeout duration when the audio is stopped in the timeline. This is useful when for example you need an audio loop to stop suddenly before playing the next one, or likewise you may want two audio loops to seamlessly transition over a few seconds.
Each global audio object can be assigned a separate audio file in the usual way which will be assigned an identifier based on the global audio object name e.g. globalBackgroundAudio1.

 

### globalaudio ###

### Save Manager ###

The save manager holds the long term information for the application. This currently records the following data:

Current page number
Roster card activated
Total number of roster cards activated
If the user has used the comic for the first time or not
We are using a framework called EasySave2 (http://docs.moodkie.com/product/easy-save-2/) which ensures the save mechanism works across multiple platforms without having to worry about configuring for each one. This object contains some special functions which allow debugging of the saved items in the app:

Press the following during runtime for save related debug options:

Space – Activates all roster cards
d – Deactivates all roster cards
c – Checks which cards are present and outputs on the console
a – Checks which cards have been activated and outputs to the console.
x – Deletes all data associated with the roster cards. It is essentially a reset.

 0 0
Back To Top

Card Target (Under Development)

This is now a deactivated feature, but it basically acted like a positional target once a roster card screen was closed by the user. The idea was that instead of the roster card disappearing into the center of the screen you could have a counter for example in the upper left hand corner and the card would move towards that as it disappears.
I may reactivate this in future when I re audit the UI.

cardTarget

 0 0
Back To Top

Submenu

The submenu is activated when the user taps the center of the screen. This submenu acts as a gateway to the other main functions of the comic such as page selection, tutorial overlay, setting, share options and the medikidz collection.
The submenu is a prefab object generated by Sub Menu scene located in the scenes folder.

Credits

The credits menu is a prefab generated by the credits scene in the scenes folder. It consists four main panels and buttons which hold dynamically sized text and images.

credits

Page selection

The page selection scene is a prefab generated from the page menu scene in the scenes folder. The menu simply allows us to select a page which the engine will then redirect us too.

pagemenu

Tutorial

The tutorial overlay is a prefab generated from the tutorial scene located in the scenes folder. This is usually called the first time the user opens the application or when the user selects the tutorial button from the sub menu. It is deactivated simply by clicking anywhere on the screen and is followed up by a secondary tutorial overlay prompt which shows specifically where to tap to progress through the comic. The secondary prompt is also a prefab generated from a prefab generated by the hint scene.



Settings

The settings menu is a prefab generated from the settings scene located in the scenes folder. Currently this settings folder only contains one main function which is to control the volume of all audio sources. In future this could be expanded to control music, sfx or voice audio individually as well as potentially other options e.g. colour blind mode.

settings

Share

The share menu is a prefab generated from the share scene. It contains buttons for sharing the application including a screenshot on facebook, twitter, email or simply onto your devices camera roll. It should be noted that this feature is disabled for webplayer and utilises the framework from Stan’s Assets to ensure multi device compatibility (https://unionassets.com/ultimate-mobile/manual). The stan assets package has a great deal more functionality which which we are currently not taking advantage off such as google analytics, in-app purchases, notifications and advertising which all interesting options to consider in future and are relatively easy to implement.

Share

Medikidz Collection (Roster Cards)

The more I look at this current title the more I think it needs to be renamed. This is again a prefab which has been generated from the HotSpotPrefab scene located in the scenes folder.
It is a menu showcasing all the collected roster cards and allowing the user to select one to look at it again. On a basic level it contains a grid with roster card thumbnail prefabs. Each of these roster card thumbnail prefabs contain all the logic for their corresponding roster card which I will describe next:

roster card menu

Roster Card Thumbnail Object

A card thumbnail object contains an image script for the card image, a button script which links to the hotspot script on the same object and the hotSpot script itself where you can assign the corresponding roster card to the thumbnail.

Currently there is a limit of 9 cards which is mostly due to my lazy programming but I will look into making this more dynamic when I learn more about class inheritance. Until then addition additional lines of code will do the job (*hears shotgun being loaded*).

rostercard

 0 0
Back To Top

Page Framework Anatomy

Page Framework Anatomy

A page scene could technically be anything, it’s simply a scene in unity which is dynamically called up by the core comic framework. But for kicks lets look at how we can take advantage of the framework to create a page in the comic. First step – create a new scene. This scene should be named and stored in a folder with the following prefix ‘ “page” + x ‘ with x being the page number. This in turn is stored under the folder pages. For example page would would look like this: pages > page1 > page1.scene and pages > page2 > page2.scene respectively. It won’t break the framework to store these files in the different way, but deviating from the naming convention will.

Screen Shot 2015-06-30 at 14.44.20

 0 0
Back To Top

Page Setup

Lets go through how to set up a comic page from scratch. Its probably best to look at some existing pages to get a feel of the makeup but here is the underlying anatomy:

Create an empty object following the naming convention ‘ “page” + x ‘ with x being the page number with no games e.g. page1. This object does nothing else which may seem silly but actually is quite useful as it allows us to move, scale and play around with an entire page’s transform parameters without buggering up any of the other settings. It also acts as a handy reference when trying to see which page has loaded into the core framework.
Childed into that top object you need to create two more objects which should always be the same – Page and Audio.
The Page object needs to have an animation, controller and audio play script loaded in.
The animation script needs to take a animation file configured to animation type 1 (which can be accessed by selecting the animation file and changing to debug mode in the inspector).
The controller script has a public variable called Page Num where would would define the page number you are currently working on.
The audio play script is where you define one off shot sound effects each referenced with a specific name in the inspector e.g. sound effect 1 etc. This audio script also has a slider variables with controls the fade out duration of the background audio loops which I will describe next.
Nested beneath the Page object is are empty panels object with the naming convention ‘ “page” + x’ with x being the panel number which would be identified from the pre planning stage e.g. panel1. Childed into this object will be all the graphical assets for that specific panel. **NOTE** All graphical assets used in the comic need to be in the sprite format.
The audio object has an empty object called music nested into it, and below that three background audio objects. These objects have name identifier e.g. background1 and within them have a standard audio script where you can drag audio files into and control some factors like looping, volume and pitch. Unlike the previous audio options in the page object above, these objects are designed to hold background music or looping tracks. **NOTE** – Make sure all your source audio assets have 3d sound disabled otherwise it won’t work.
Alternatively when setting up a new page you can simply clone an existing page and replace everything nested under the panel gameobject with new page content.

pagesetup

 0 0
Back To Top

Animation

There are some ground rules for animation.

Make sure all animation is created on the Page object in the hierarchy.
Currently we are only using legacy animation files. This can be set by clicking on an animation file and selecting the debug option from inspector and set the animation type to 1.
Animation for the panels is created in the standard unity animation editor. It is important that you have all your graphical assets setup before starting any animation.
Refer to the animation guide (separate document) which talks about the guidelines for creating the medikidz motion graphic style.
Ensure all animation is set up to ensure compatibility with multiple device screen ratios in the landscape orientation.
The animation has the ability to call special trigger events on the timeline which control things like, page transition, when the timeline should stop, making a hotspot appear and also triggering audio. The main ones we will be using are as follows:

Stopper() – Pauses the animation. You would normally place a stopper() when a panel is in full view. The user must click the right or left navigation areas in order to progress to the next or previous panel respectively.
frameStart() – You will only need to use this once at the start of each page animation to define where on the animation timeline should the animation start.
frameBounce() – Usually would be placed on the timeline on the first page before the frameStart() trigger. Should the user go backwards on the first page this will send themforwards again.
HotSpotCreate() – We will talk about creating a hot spot in a little bit, but this is the trigger that lets the page know when it’s ok to make it appear on screen.
soundEffect’x’_audio() – Plays a single one shot sound effect which corresponds to the audio files placed in the local Page object.
background’x’_audio() – Plays a background loop which corresponds to the audio file placed in the local background’x’ object under Audio > Music
background’x’_fadeout() – Trigger which tells the system to start fading out and then stop the background loop which corresponds to the audio file placed in the local background’x’ object under Audio > Music
playGlobalBackgroundAudio’x’() – Plays a global background audio file which are set up in the comic framework. The number ‘x’ corresponds to the number of the assigned audio file object. See global audio section for more details.
stopGlobalBackgroundAudio’x’() – Fade out and stops global audio playing which corresponds to the value of x.
animation event

 0 0
Back To Top

Audio Control

We spoke above about how to call an audio trigger event on the animation timeline, so lets talk about how to assign a local audio source.

First you must load in all the audio files for a particular page into an audio folder located in its respective page folder e.g pages > page2 > audio

**NOTE** – Make sure all your source audio assets have 3d sound disabled otherwise it won’t work.

Local Audio One Shot

Select the Page object and go to the audio play script in the inspector. There you will see 6 empty slots named sound effect 1 – 6. Slot in here all your one shot audio clips i.e. the audio that is not meant to be looped like sound effects. The number here will correspond to the audio trigger event on the timeline using soundEffect’x’_audio() with ‘x’ being the number.

Local Audio Background Loops

Locate the background’x’ with x being the number object in the hierarchy located under page > Audio > Music. Here you will find three dedicated background objects. Clicking on these will bring up an audio source script controller which you can view in the inspector. Here is where you can drop your audio clip and have access to the specialized functions such as loop, volume, pitch and 2d panning. The number of the background object ‘x’ will correspond to the audio event trigger background’x’_audio() located on the timeline.

audio

 0 0
Back To Top

Hot Spot Placement

The roster card object is really a placeholder to mark the position of where on the screen the hotspot object should be instantiated. Drag in the Hot Spot Placement prefab located in the resources folder into your scene. Its important to note that this object should not be hierarchized under the page’x’ object; it could be but to make things more flexible for us and not have to worry about animation conflicts place it separately unless of course you want it to move with the animation in a particular way.
Position the roster card placement marker in your scene using the standard unity translate tools on the and and y axis. Be sure to check the hotspot placement won’t be clipped off at different screen aspect ratios. Clicking on the hot spot placement object reveals a hot spot instantiate script in the inspector which allows you to select which roster card this hotspot corresponds to.hotspot

 0 0
Back To Top

Page Selection

Page Selection

The page menu object contains most of the logic for all the buttons including animation control. Within the hierarchy are a group of buttons which have an image and click logic drawing from the buttonClickControl script on the same object. The buttonClickControl script on each object needs to have the associated page number defined so it knows which page to open. You also have the option of applying a unique close sound clip for a particular page.
You can easily add additional page buttons by cloning one of the existing page buttons or equally remove them. The dynamic grid will automatically update itself to compensate.

Page Selection

 0 0
Back To Top

Roster Cards

Roster Cards

Roaster cards are one of the few 3D elements of the engine. These cards are constructed from a flat plane which dynamically loads a texture depending on which roster card has been selected. The player can collect roster cards by selecting hotspots within the comic and later view them again in the Medikidz Collection section located under the submenu.

To create a roster card we first must open the roster card scene. Select the roster card object from the hierarchy and look at the roster card script in the inspector. Here you will find nine slots for different roster card textures with each texture corresponding to a variable i.e. Card01.
You may notice some other variables here too like the ‘card target’ which links up with the missing card target object feature I mentioned before in the comic framework chapter (I may get round to re-enabling this in future) which provides a tagret point when the roster card plays its disappearing animation.
Once your card has been fully assigned simply create a prefab of it in the resources folder.

rostercard

 0 0
Back To Top

Graphical Formatting Guidelines

Graphical Formatting Guidelines

Images

It goes without saying that any comic book is very heavy on visuals. In fact you could argue the visuals are more important than the narrative of a comic (not at Medikidz of course). That said, these comic panels are made up of highly detailed, memory intensive and storage space hungry graphics which we have to manage to ensure maximum compatibility on a range of devices. Its not particularly hard to do, but does require you to vigilant about how much storage space a single graphical asset is taking up and to be prepared to do quite a bit of prep work. First some background on how unity handles images.

Unity & Graphical Assets

IMPORTANT – All graphical assets used in the comic must be exported using the power of two rule to ensure maximum image compression (especially on iOS devices). Learn more about unity and textures here – http://docs.unity3d.com/Manual/class-TextureImporter.html

Try to export at the maximum resolution that you can, usually at png-24 (8 if there is no transparency) and don’t worry about performing any specialised png compression as unity ignores it anyway.

All our textures are handled as sprites within unity. When creating your graphical assets you have the choice of putting a single file for a single sprite or packing them together into a sprite sheet. I would advise against using too many sprite sheets unless you know they will be all used on the same page, otherwise a situation may arise where the framework has to load three or four large sprite sheets just to grab a couple of assets and can lead to crashes. I would actually recommend that you change to the build for iOS in the build settings so you are able to accurately see the estimated image file size in the preview window under inspector.

Backgrounds

For all newly imported graphical assets we should be aiming for the lowest resolution possible without significantly impacting on the quality. This typically will be setting Max Size to 1024 and the Format to ‘Compressed’ under the defaults. Also be sure to disable mipmaps in the inspector. Rarely we can go up to 2048 for very large backgrounds but try not to use this too often (2-3 times per comic). Typically a single background or large graphical asset should not be more than 0.5mb after being imported by unity (be sure to check the preview window of the inspector once you have selected the image in question to check).

Speech Bubbles

We currently export all our speech bubbles and text boxes as png-24 directly from the comic. Again, the maximum size should be 512px (1024px rarely if significant dithering creeps in), compressed with mip maps turned off.

 0 0
Back To Top

Converting a Medikidz Comic to Digital

Converting a Medikidz Comic to Digital

This process describes the main method used internally at Medikidz to convert an existing layered comic from its illustration/photoshop files into a format suitable for the comic framework. This step assumes that a formal review of the comic has already been conducted by the animator and that any art amends required have been made.

Exporting comic panels

Medikidz comics are composed of two parts – psd’s of each page within a comic and illustrator files which contain all the speech bubbles and text boxes.

Comic Page

Comic pages of composed of panels which in turn are composed of foreground, midground, backgrounds and character elements on different layers. Its important to go through the layers and make sure they are named appropriately as this will save time later on make it easier to swap assets if required in future.
You then have the choice to export each layer as a png-24 ensuring that each file dimensions meets the power of two requirement and that the naming convention is the same as the psd with a reference to the page number and panel number – e.g. p2_panel2_bg = page 2, panel2 background.
Alternatively, you can use a piece of software like Slicy (http://macrabbit.com/slicy/) which automatically exports a single png for each layer. You will have to go back and edit these files though to make sure they meet the power of 2 requirement (I have written a photoshop script for this which can quickly batch process a whole bunch of images at once).

Speech bubbles & text boxes

All our speech bubbles and text boxes are kept in a separate illustrator file. We recommend slicing these up in illustrator and exporting them as png-24. I would also recommend you scale up the image by adjusting the settings in the save for web preview screen to 600%.
The naming convention of these exported slices should also be easily identifiable e.g. MS_AU_Page__10_03 = multiple sclerosis, australia, page 10, bubble 3 (bubbles are numbered based on sequence read on the page).

Unity File System

Within unity all individual pages are stored under the pages folder e.g. pages > page2. All the graphical assets, 3d models, audio and animation related to that page should be stored in that specific page folder to prevent confusion and quick navigation.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
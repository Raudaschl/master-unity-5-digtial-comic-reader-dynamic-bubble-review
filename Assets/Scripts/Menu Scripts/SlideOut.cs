using UnityEngine;
using System.Collections;

public class SlideOut : MonoBehaviour {
	
	protected Animator animator;
	private GameObject[] centerNavs;
	private GameObject[] portraitNavs;
	private GameObject[] hotspotInteractives;
	private GameObject rosterCard;
	private GameObject panelNavigation;

	private GameObject RosterCardMenu;

	private int PageNumber;
	private GameObject thePageNum;
	public GameObject thePageNumText;

	private GameObject rosterCounter;

	public GameObject mainCamera;
	private GameObject canvasNav;


	public AudioClip openSound;

	public AudioClip buttonClick;


	
	void Start() {

		RosterCardMenu = GameObject.Find ("RosterCardMenu");

		//Set up arrays of objects based on TAG
		centerNavs = GameObject.FindGameObjectsWithTag("centerNav");

		portraitNavs = GameObject.FindGameObjectsWithTag("Portrait");

		panelNavigation = GameObject.Find ("PanelNav");

		buttonClick = Resources.Load ("Audio/UI/Menu-Interface-8")as AudioClip;

		mainCamera = GameObject.Find ("Main Camera");

		
		animator = GetComponent<Animator> ();
		if(animator != null)
		{
			animator.applyRootMotion = false;
		}
		
	}

	//Navigagtion Renaming
	void centerNavLayerName(string theName){


		for (int index = 0; index < centerNavs.Length; index++)
		{
			
			var centerNav = centerNavs[index];
			centerNav.layer = LayerMask.NameToLayer (theName);

		}

	}
	

	void portraitNavLayerName(string theName){

		for (int index = 0; index < portraitNavs.Length; index++)
		{
			
			var portraitNav = portraitNavs[index];
			portraitNav.layer = LayerMask.NameToLayer (theName);
			
		}

	}
	
	void hotspotNavLayerName(string theName){

		for (int index = 0; index < hotspotInteractives.Length; index++)
		{
			
			var hotspotInteractive = hotspotInteractives[index];
			hotspotInteractive.layer = LayerMask.NameToLayer (theName);
			
		}

	}



	//End name tagging

	public void TriggerOut(){

		hotspotInteractives = GameObject.FindGameObjectsWithTag ("HotSpot");
		
		//Turn back on raycast layer for nav
		
		


		
		centerNavLayerName("Default");


		hotspotNavLayerName("Default");

		panelNavigation.GetComponent<CanvasActivationControl> ().EnableCanvas ();
		
		//Play aniamtion
		
		animator = GetComponent<Animator> ();
		animator.SetBool ("SlideIn", false);
		animator.SetBool ("SlideOut", true);


	}





	
	public void TriggerIn(){

//		GetComponent<Animator>().enabled = true;	

		GetComponent<AudioSource>().PlayOneShot(openSound);

		hotspotInteractives = GameObject.FindGameObjectsWithTag("HotSpot");
		
		//Set Layer to ignore raycast
		

		centerNavLayerName("Ignore Raycast");
		

		
		portraitNavLayerName("Ignore Raycast");

		hotspotNavLayerName("Ignore Raycast");
		
		
		animator = GetComponent<Animator> ();
		animator.SetBool ("SlideOut", false);
		animator.SetBool ("SlideIn", true);
		
		
		//Page thumbnails animation load in
		GameObject Hot = GameObject.Find("Cover");
		Hot.GetComponent<buttonClickControl>().appear();
	}


	//Instructions Panel - Slide In

	public void TriggerInInstructions(){
		GetComponent<Animator>().enabled = true;
		
		hotspotInteractives = GameObject.FindGameObjectsWithTag("HotSpot");
		
		//Set Layer to ignore raycast
		
		
		
		centerNavLayerName("Ignore Raycast");
		

		
		portraitNavLayerName("Ignore Raycast");
		
		hotspotNavLayerName("Ignore Raycast");
		
		
		animator = GetComponent<Animator> ();
		animator.SetBool ("MoveOut", false);
		animator.SetBool ("MoveIn", true);
		animator.SetBool ("Begin", true);
	}


	//Instructions Panel - Slide Out

	public void InstructionsTriggerOut(){
		
		panelNavigation.GetComponent<CanvasActivationControl> ().EnableCanvas ();
		hotspotInteractives = GameObject.FindGameObjectsWithTag ("HotSpot");
		
		//Turn back on raycast layer for nav
		
		
		
		

		centerNavLayerName("Default");

		portraitNavLayerName("Default");
		
		hotspotNavLayerName("Default");
		
		//Play aniamtion
		
		animator = GetComponent<Animator> ();
		animator.SetBool ("MoveOut", true);
		animator.SetBool ("MoveIn", false);
	}

	//Roster Card Menu Panel - Slide in
	
	public void TriggerInRosterMenu(){
//		GetComponent<Animator>().enabled = true;
		
		hotspotInteractives = GameObject.FindGameObjectsWithTag("HotSpot");
		
		//Set Layer to ignore raycast
		
		
		
		centerNavLayerName("Ignore Raycast");

		
		portraitNavLayerName("Ignore Raycast");
		
		hotspotNavLayerName("Ignore Raycast");
		
		
		animator = GetComponent<Animator> ();
		animator.SetBool ("TriggerOut", false);
		animator.SetBool ("TriggerIn", true);
		animator.SetBool ("Begin", true);
	}

	//Roster Card Menu - Slide Out
	
	public void TriggerOutRosterMenu(){
		

		hotspotInteractives = GameObject.FindGameObjectsWithTag ("HotSpot");
		rosterCard = GameObject.Find ("Roster Card");



		//Check if card is activated
		if (rosterCard != null) {


//				if card present and vibrating animation is on then when the exit button is pressed it will just close the card

				if (rosterCard.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName ("vibrate")) {

			
					rosterCard.GetComponent<RosterCard> ().EndCardFromMenu ();

				return;


//				if card is not using vibrating animation is off then when the exit button is pressed it will close the menu
				} else {

				rosterCardMenuSubTriggerOut();
				}


//				if card is not present then when the exit button is pressed it will close the menu
		} else {
			
			rosterCardMenuSubTriggerOut();
		}

	}

	//Subroutine for TriggerOutRosterMenu()

	void rosterCardMenuSubTriggerOut(){

		panelNavigation.GetComponent<CanvasActivationControl> ().EnableCanvas ();


		mainCamera.GetComponent<SwipeDetector>().enabled = true;
		
		//Turn back on raycast layer for nav
		

		centerNavLayerName("Default");


		portraitNavLayerName("Default");
		
		
		hotspotNavLayerName("Default");

		//Play aniamtion
		
		animator = GetComponent<Animator> ();
		animator.SetBool ("TriggerOut", true);
		animator.SetBool ("TriggerIn", false);
	}

	
	// Slide in sub menu
	public void TriggerInSubMenu(){

		GetComponent<AudioSource>().PlayOneShot(openSound);
		
		hotspotInteractives = GameObject.FindGameObjectsWithTag("HotSpot");

		//Set Layer to ignore raycast
		
		
		centerNavLayerName("Ignore Raycast");

		
		portraitNavLayerName("Ignore Raycast");

		hotspotNavLayerName("Ignore Raycast");
		
		//play animation
		animator = GetComponent<Animator> ();
		animator.SetBool ("slide in", true);
		animator.SetBool ("slide out", false);




//		pageNumSubMenu ();

		//Destroy hint system

		if (GameObject.Find("HintSystem") != null)
		{
			GameObject hintSystem = GameObject.Find("HintSystem");
			hintSystem.GetComponent<destroyObject>().Destroy();
		}
		
	}
	
	// Slide out sub menu
	public void TriggerOutSubMenu(){

	
		panelNavigation.GetComponent<CanvasActivationControl> ().EnableCanvas ();

		hotspotInteractives = GameObject.FindGameObjectsWithTag("HotSpot");
		
		//Set Layer to ignore raycast
		
		
		centerNavLayerName("Default");
		
		portraitNavLayerName("Default");

		hotspotNavLayerName("Default");
		
		
		//play animation
		animator = GetComponent<Animator> ();
		animator.SetBool ("slide out", true);
		animator.SetBool ("slide in", false);
		
		
	}

//Activate thumbnail menu

	public void activateThumbnailMenu(){
		
		TriggerOutSubMenu ();


		panelNavigation.GetComponent<CanvasActivationControl> ().DisableCanvas ();
		
		GameObject Hot = GameObject.Find ("Page Menu");
		Hot.GetComponent<SlideOut> ().TriggerIn ();

		GetComponent<AudioSource>().PlayOneShot(buttonClick);
		
		
	}

	//Activate Settings menu
	
	public void activateSettingsMenu(){

		TriggerOutSubMenu ();

		GameObject SettingsObject = GameObject.Find ("Settings");
		SettingsObject.GetComponent<SlideOut> ().TriggerIn ();


		panelNavigation.GetComponent<CanvasActivationControl> ().DisableCanvas ();

		GetComponent<AudioSource>().PlayOneShot(buttonClick);

	}

	//Activate Main menu Exit Popup
	
	public void activateExitPopup(){
		
		TriggerOutSubMenu ();
		
		GameObject ExitPopup = GameObject.Find ("Exit Popup");
		ExitPopup.GetComponent<SlideOut> ().TriggerIn ();
		
		
		panelNavigation.GetComponent<CanvasActivationControl> ().DisableCanvas ();

		checkPageNumber ();

		GetComponent<AudioSource>().PlayOneShot(buttonClick);
		
	}


	//Activate Share SubMenu
		
	public void activateShareSubMenu() {
		
		TriggerOutSubMenu ();
		
		GameObject ShareSubMenu = GameObject.Find ("Share SubMenu");
		ShareSubMenu.GetComponent<SlideOut> ().TriggerIn ();
		
		
		panelNavigation.GetComponent<CanvasActivationControl> ().DisableCanvas ();

		GetComponent<AudioSource>().PlayOneShot(buttonClick);
		
	}

	//Activate Credits menu
	
	public void activateCreditsMenu(){
		
		TriggerOutSubMenu ();
//		
		GameObject CreditsObject = GameObject.Find ("Credits");



		if (CreditsObject != null) {

			CreditsObject.GetComponent<SlideOut> ().TriggerIn ();
		} else {
		
			Debug.Log ("Credits object not found");
		}
//		
//		
//		panelNavigation.GetComponent<CanvasActivationControl> ().DisableCanvas ();
//
//		//Inactivate second swipe script
//	
////		Debug.Log(mainCamera);
//
//
//		mainCamera.GetComponent<SwipeDetector>().enabled = false;
//
//		GetComponent<AudioSource>().PlayOneShot(buttonClick);
		
	}

	//Activate Instructions menu
	
	public void activateInstructionsMenu(){
		
		TriggerOutSubMenu ();
		
		
//		panelNavigation.GetComponent<CanvasActivationControl> ().DisableCanvas ();

		//Tutorial
		GameObject Tutorial = (GameObject)Instantiate(Resources.Load("Tutorial"));
		
		Tutorial.name = Tutorial.name.Replace("(Clone)", "");
panelNavigation.GetComponent<CanvasActivationControl> ().EnableCanvas ();
//		GameObject Instructions = GameObject.Find ("Instructions");
//		Instructions.GetComponent<SlideOut> ().TriggerInInstructions ();

		GetComponent<AudioSource>().PlayOneShot(buttonClick);
		
		
	}

	//Activate Roster Card Thumbnail menu
	
	public void activateRosterMenu(){
		
		TriggerOutSubMenu ();

		//Refeshes roster thumbs to check for activation
		gameObject.GetComponent<rosterThumbActivateCheck> ().refreshRosterThumbActivation ();
		//

		panelNavigation.GetComponent<CanvasActivationControl> ().DisableCanvas ();

		RosterCardMenu = GameObject.Find ("RosterCardMenu");

		RosterCardMenu.GetComponent<SlideOut> ().TriggerInRosterMenu ();

//		Currently deactivated until we bring back the roster card counter feature

//		GameObject numRosterCards = GameObject.Find("rosterCounter2");
//		numberOFActivatedCards playerScript = numRosterCards.GetComponent<numberOFActivatedCards>();
//			playerScript.checkNumberOFCards ();

		GetComponent<AudioSource>().PlayOneShot(buttonClick);
		
		
	}
	

	
	public void ActivatePageMenu(){
		
//		GetComponent<Animator>().enabled = true;
		
		GameObject SubMenu = GameObject.Find ("SubMenu");
		SubMenu.GetComponent<SlideOut> ().TriggerOutSubMenu ();
		
		
		
		centerNavLayerName("Ignore Raycast");


		GetComponent<AudioSource>().PlayOneShot(buttonClick);
		
	}
	
	public void ActivateMidNav(){
		
		
		//Set Layer to ignore raycast
		
		
		centerNavLayerName("Default");

		
	}

	public void inactivateMainNav() {
		centerNavLayerName("Ignore Raycast");
		

		
		portraitNavLayerName("Ignore Raycast");
		Deactive ();




		//Inactivate swipe script

		canvasNav = GameObject.Find ("CanvasNav");
		
		canvasNav.GetComponent<touchControlCenter>().enabled = false;

		//Inactivate second swipe script

		
		mainCamera.GetComponent<SwipeDetector>().enabled = false;

	}
	
	public void Deactive(){
	
		panelNavigation.GetComponent<CanvasActivationControl> ().DisableCanvas ();
	}

	public void Activate(){

		RosterCardMenu = GameObject.Find ("RosterCardMenu");

		if (RosterCardMenu.GetComponent<Animator> ().GetBool ("TriggerIn") == false) {

						panelNavigation.GetComponent<CanvasActivationControl> ().EnableCanvas ();
				}
	}

	public void ActivateSwipeScript() {
		//Activate swipe script
		
		canvasNav = GameObject.Find ("CanvasNav");
		
		canvasNav.GetComponent<touchControlCenter>().enabled = true;


		
		mainCamera.GetComponent<SwipeDetector>().enabled = true;
	}

	//What Page is this bitch? (sorry-watching too much breaking bad)

	public void checkPageNumber(){
		//Find page number 

		thePageNum = GameObject.Find("Page");
		controller playerScript = thePageNum.GetComponent<controller>();
		PageNumber = playerScript.pageNum;
		Debug.Log (PageNumber);
		ES2.Save (PageNumber, "medikdizComic.txt?tag=pageNumber");

	}


	//Shows page number and number of collected roster cards
	public void pageNumSubMenu(){
		
		thePageNum = GameObject.Find("Page");
		PageNumber = thePageNum.GetComponent<controller>().pageNum;




		thePageNumText = GameObject.Find("pageNumText");
		thePageNumText.GetComponent<setPageNum> ().setPageNumText ();

//		rosterCounter = GameObject.Find("rosterCounter");
//		rosterCounter.GetComponent<numberOFActivatedCards> ().checkNumberOFCards ();

	}


	
}

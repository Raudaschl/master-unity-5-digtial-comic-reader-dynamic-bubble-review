﻿using System;
using UnityEngine;
using System.Collections;


public class loading_prefab : MonoBehaviour {
	
	public GameObject page;
	public static int Direction = 2;
	public AssetBundle bundle;
	
	public enum pageLoadMode {Manual, Server, WebPlayer};
	public pageLoadMode loadType;
	
	public enum cleanCache {No, Yes};
	//True = Cleans out the cache
	public cleanCache cleanCacheChoice; 
	
	public int numPageNumber;
	
	public string BundleURL;
	public string PageNumber;
	private string GlobalAudioAddress = "Global Audio";
	protected GameObject globalAudio;
	
	public int version;
	
	public bool comicSaveFileOveride;
	
	public static float pageCount = 30;

	private GameObject PageMenu;
	private GameObject RosterCardMenu;
	
	// Use this for initialization
	void Start () {
		
		if (comicSaveFileOveride == true) {
			if (ES2.Exists ("medikdizComic.txt?tag=comicToLoad")) 
			{
				Debug.Log ("exists");
				BundleURL = ES2.Load<string>("medikdizComic.txt?tag=comicToLoad");
			} else {
				Debug.Log ("does not exist");
			}
			
		}
		
		if (cleanCacheChoice == cleanCache.Yes) {
			cleanAssetBundleCache();
		}
		
		//		//Load First Page of Comic
		
		if (loadType == pageLoadMode.Server){

			StartCoroutine (DownloadAndCache());
		} else {
			
			offlineSceneLoad();
		}
		
		//		END COMIC LOADING
		
		
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		
		
		
		
		
		//Load Sub Menu
		GameObject SubMenu = (GameObject)Instantiate(Resources.Load("SubMenu"));
		
		SubMenu.name = SubMenu.name.Replace("(Clone)", "");


		

		
		//Settings Menu
		GameObject Settings = (GameObject)Instantiate(Resources.Load("Settings"));
		
		Settings.name = Settings.name.Replace("(Clone)", "");
		
		//		//Instructions Menu
		//		GameObject Instructions = (GameObject)Instantiate(Resources.Load("Instructions"));
		//		
		//		Instructions.name = Instructions.name.Replace("(Clone)", "");
		

		

		//Exit popup Menu
		GameObject ExitPopup = (GameObject)Instantiate(Resources.Load("Exit Popup"));
		
		ExitPopup.name = ExitPopup.name.Replace("(Clone)", "");
		
		//Share Sub Menu
		GameObject ShareSubMenu = (GameObject)Instantiate(Resources.Load("Share SubMenu"));
		
		ShareSubMenu.name = ShareSubMenu.name.Replace("(Clone)", "");
		
		
		
		
		
		//Fader
		GameObject Fader = (GameObject)Instantiate(Resources.Load("FadeInOut"));
		
		Fader.name = Fader.name.Replace("(Clone)", "");
		
		
		
		
		
		//Camera and Navigation Prefabs
		
		if (checkIfCameraNavExists () == false) {
			GameObject camera = (GameObject)Instantiate (Resources.Load ("Camera_Nav"));
			
			camera.name = camera.name.Replace ("(Clone)", "");
		}
		
		Resources.UnloadUnusedAssets();
		
		
		
		
		//		page.name = page.name.Replace("(Clone)", "");
		
		
		
		//		//Comic Load
		//		GameObject comicPrefab = (GameObject)(Resources.Load("scoliosis"));
		//
		
		
	}
	
	
	IEnumerator DownloadAndCache (){

			BundleURL = "http://www.medikidz.com/assetBundle/comicpages." + BundleURL;
		// Wait for the Caching system to be ready
		while (!Caching.ready)
			yield return null;
		
		// Load the AssetBundle file from Cache if it exists with the same version or download and store it in the cache
		using (WWW www = WWW.LoadFromCacheOrDownload (BundleURL, version)) {
			
			yield return www;
			if (www.error != null)
				throw new Exception ("WWW download had an error:" + www.error);
			bundle = www.assetBundle;




			if (PageNumber == ""){
				Instantiate (bundle.mainAsset);

			}else{
				;
				//Load Scene
				
				//load global audio object
				
				 if (loadType == pageLoadMode.Server) {
					
					Debug.Log (bundle.Contains("RosterCardMenu"));


					
					if (checkIfPageExists () == false) {

			
						//Load Page Menu
						if (bundle.Contains("Page Menu") == true) {
							AssetBundleRequest requestPagemenu = bundle.LoadAssetAsync("Page Menu", typeof(GameObject));
							
							yield return requestPagemenu;
							
							PageMenu = requestPagemenu.asset as GameObject;
							PageMenu = (GameObject)Instantiate(PageMenu);
							PageMenu.name = PageMenu.name.Replace("(Clone)", "");
						}
						
						//Load Roster Card Menu

						if (bundle.Contains("RosterCardMenu") == true) {
							
							AssetBundleRequest requestRosterCardMenu = bundle.LoadAssetAsync("RosterCardMenu", typeof(GameObject));
							//Roster Card Menu
							yield return requestRosterCardMenu;
							
							RosterCardMenu = requestRosterCardMenu.asset as GameObject;
							RosterCardMenu = (GameObject)Instantiate(RosterCardMenu);
							RosterCardMenu.name = RosterCardMenu.name.Replace("(Clone)", "");
						}


						
//						Debug.Log (bundle.Contains(GlobalAudioAddress));
						//load global audio object
						if (globalAudioCheck () == false && bundle.Contains(GlobalAudioAddress) == true) {


							Instantiate (bundle.LoadAsset (GlobalAudioAddress));
						}

						AssetBundleRequest requestPage = bundle.LoadAssetAsync("page"+PageNumber, typeof(GameObject));
						//Roster Card Menu
						yield return requestPage;
						
						page = requestPage.asset as GameObject;
						page = (GameObject)Instantiate(page);
						page.name = page.name.Replace("(Clone)", "");



//						page = (GameObject)Instantiate(bundle.LoadAsset ("page"+PageNumber));
						numberOfPagesServer();
						pageNameClean();
					}
					
					// Unload the AssetBundles compressed contents to conserve memory
//					bundle.Unload (false);
					// memory is freed from the web stream (www.Dispose() gets called implicitly)
					
					
					
				} else {
					
					Debug.Log ("Oh dead nothing seemed to load");
				}
				
			}
		}
	}



		
		//Find number of pages for server
	void numberOfPagesServer(){
		pageCount = 0;
		for (int i = 0; i < bundle.GetAllAssetNames().Length; i++){
			
			string assetName = bundle.GetAllAssetNames()[i];
			
			if (assetName.Contains("page")){
				pageCount++;

			}
			
		}
	
	}

	//Find number of pages for local
	void numberOfPagesLocal(){
		pageCount = 1;
//		Debug.Log (Resources.FindObjectsOfTypeAll(typeof(GameObject)).Length);
		for (int i = 0; i < Resources.FindObjectsOfTypeAll(typeof(GameObject)).Length; i++){


//			Debug.Log (Resources.FindObjectsOfTypeAll(typeof(Component))[i].name);
			GameObject assetName = Resources.FindObjectsOfTypeAll(typeof(GameObject))[i] as GameObject;
	


			if (assetName.name == "Page"){
				
				pageCount++;
				Debug.Log (pageCount);
			}
			
		}

	}
	
	void offlineSceneLoad(){
		//load global audio object
		
		//Load Page Menu
		PageMenu = (GameObject)Instantiate(Resources.Load("Page Menu"));
		
		PageMenu.name = PageMenu.name.Replace("(Clone)", "");
		
		//Roster Card Menu
		RosterCardMenu = (GameObject)Instantiate(Resources.Load("RosterCardMenu"));
		
		RosterCardMenu.name = RosterCardMenu.name.Replace("(Clone)", "");

		//Credits Menu
		GameObject CreditsMenu = (GameObject)Instantiate(Resources.Load("Credits"));
		
		CreditsMenu.name = CreditsMenu.name.Replace("(Clone)", "");

		
		
		if (loadType == pageLoadMode.WebPlayer) {
			
			
			
			//load global audio object
			if (globalAudioCheck () == false) {
				Instantiate(Resources.Load(GlobalAudioAddress));
			}
			
			Application.LoadLevelAdditive("page"+numPageNumber);
			
			
			
		} else if (loadType == pageLoadMode.Manual) {
			
			
			
			if (globalAudioCheck () == false) {
				Instantiate(Resources.Load(GlobalAudioAddress));
			}
			
			page = (GameObject)Instantiate(Resources.Load("page"+numPageNumber));
			numberOfPagesLocal();
			pageNameClean();
		} 
			

		
		
	}
	
	
	//Check if global audio object is in the scene
	public bool globalAudioCheck(){
		
		if (GameObject.Find ("Global Audio(Clone)")) {
			globalAudio = GameObject.Find ("Global Audio(Clone)");
			globalAudio.name = globalAudio.name.Replace("(Clone)", "");
			//			Debug.Log ("Assigned");
			return true;
		} else if (GameObject.Find ("Global Audio")) {
			globalAudio = GameObject.Find ("Global Audio");
			//			Debug.Log ("Assigned2");
			return true;
		} else {
			//			Debug.Log ("ERROR! No Global Audio Object Found");
			return false;
		}
		
	}
	
	public bool checkIfPageExists(){
		if (GameObject.FindGameObjectWithTag ("Page")) {
			return true;
		} else {
			return false;
		}
		
	}
	
	public bool checkIfCameraNavExists(){
		if (GameObject.FindGameObjectWithTag ("CameraNav")) {
			return true;
		} else {
			return false;
		}
		
	}
	
	
	public void pageNameClean(){
		page.name = page.name.Replace("(Clone)", "");
	}
	
	public void cleanAssetBundleCache(){
		Caching.CleanCache ();
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class rosterThumbActivateCheck : MonoBehaviour {

	public int checkActive;
	private string buttonName ;

	private GameObject[] numberOfRosterThumbs;



	// Use this for initialization
	void Start () 
	{
;

		buttonName = gameObject.name;

		if (ES2.Exists ("medikdizComic.txt?tag=" + buttonName)) {
						rosterCardThumbActivateCheck ();
				}
	}


	//Refeshes number of roster thumbs to check for activation

	public void refreshRosterThumbActivation(){
		numberOfRosterThumbs = GameObject.FindGameObjectsWithTag("RosterThumbs");

//		foreach (GameObject numberOfRosterThumb in numberOfRosterThumbs) {
//			numberOfRosterThumb.GetComponent<rosterThumbActivateCheck> ().rosterCardThumbActivateCheck();
//			
//		}

		for (int index = 0; index < numberOfRosterThumbs.Length; index++)
		{
			
			var numberOfRosterThumb = numberOfRosterThumbs[index];
			numberOfRosterThumb.GetComponent<rosterThumbActivateCheck> ().rosterCardThumbActivateCheck();

				
			} 

	
	}

	public void rosterCardThumbActivateCheck()
	{
		// Load the position we saved and assign it to this GameObject.

		buttonName = gameObject.name;
		checkActive = ES2.Load<int>("medikdizComic.txt?tag="+buttonName);


		//Count total number of cards by getting each activated object to contribute



//		Debug.Log (buttonName);

		if (checkActive == 1) 
		{
			activateButton();
				} else {
			deactivateButton();
		}


	
	}


	//Decativates current card thumbnail button
	 void deactivateButton()
	{
		
		gameObject.GetComponent<Button>().interactable = false;
	}
	
	//Activates current button
	public void activateButton()
	{
		
		gameObject.GetComponent<Button>().interactable = true;
	}


}

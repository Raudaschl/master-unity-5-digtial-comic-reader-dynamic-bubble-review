﻿using UnityEngine;
using System.Collections;

public class introSounds : MonoBehaviour {

	
	public AudioClip soundEffect1;
	public AudioClip soundEffect2;
	public AudioClip soundEffect3;
	public AudioClip soundEffect4;
	public AudioClip soundEffect5;
	public AudioClip soundEffect6;

	public GameObject Abacus;


	
	//Sound Effect Controllers
	public void click_audio() {
		GetComponent<AudioSource>().PlayOneShot(soundEffect1);
		
	}
	
	public void soundEffect2_audio() {
		GetComponent<AudioSource>().PlayOneShot(soundEffect2);
	}
	
	public void soundEffect3_audio() {
		GetComponent<AudioSource>().PlayOneShot(soundEffect3);
	}
	
	public void soundEffect4_audio() {
		GetComponent<AudioSource>().PlayOneShot(soundEffect4);
	}
	
	public void soundEffect5_audio() {
		GetComponent<AudioSource>().PlayOneShot(soundEffect5);
	}
	
	public void soundEffect6_audio() {
		GetComponent<AudioSource>().PlayOneShot(soundEffect6);
	}
	
	public void abacusLeave(){
		Animator animator = Abacus.GetComponent<Animator>();
		animator.SetBool("abacus_leave", true);

	}

	 

}

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class menuImageSwapButton : MonoBehaviour {


	public int textureSelect;
	public GameObject mainImage;
	public GameObject mainButtons;
	public GameObject mainScene;
	public GameObject Logo;
	public GameObject FadeInOut;
	public GameObject startGameButton;
	public GameObject skipCutsceneButton;
	public Text instructions;

	public string[] LevelName; 

	public int gameSelect;


	protected Animator animator;

	public AudioClip buttonBack;
	
	public AudioClip buttonClick;

	public void gameMenuActivate(){

		animator = GetComponent<Animator> ();
		animator.SetBool ("SlideActivate", true);
	}
	
	public void gameMenuDeactivate(){

		animator = GetComponent<Animator> ();
		animator.SetBool ("SlideActivate", false);
		
	}

	public void imageComic(){
		textureSelect = 0;
	}

	public void imageSpotProtein(){
		GetComponent<AudioSource>().PlayOneShot(buttonClick);
		mainImage.GetComponent<menuImageSwap> ().imageSpotProtein ();
		textureSelect = 1;

		FadeInOut.GetComponent<FadeController> ().gameSelect = 1;

		instructions.text = "Find all the HIGH protein foods before the time runs out!";


		activateStartGameButton ();

	}

	public void imageProteinDodge(){
		GetComponent<AudioSource>().PlayOneShot(buttonClick);
		mainImage.GetComponent<menuImageSwap> ().imageProteinDodge ();
		textureSelect = 2;

		FadeInOut.GetComponent<FadeController> ().gameSelect = 0;


		instructions.text = "Abacus is collecting LOW protein fruits and vegetables. Can you avoid the HIGH protein foods?";
		activateStartGameButton ();


	}

	public void imageJigsaw(){
		GetComponent<AudioSource>().PlayOneShot(buttonClick);
		mainImage.GetComponent<menuImageSwap> ().imageJigsaw ();
		textureSelect = 3;
		FadeInOut.GetComponent<FadeController> ().gameSelect = 2;



		instructions.text = "Axon is trying to solve his jigsaw puzzles. Can you help him?";
		activateStartGameButton ();
	}

	public void returnToMainMenu(){
		GetComponent<AudioSource>().PlayOneShot(buttonBack);
		mainImage.GetComponent<menuImageSwap> ().imageComic ();

		gameMenuDeactivate ();

		mainButtons.GetComponent<menuButtonControl> ().slideInButton ();
		textureSelect = 0;
		instructions.gameObject.SetActive(false);	//AH Check this is correct used to be instructions.active
		startGameButton.SetActive(false);
		skipCutsceneButton.SetActive(false);



	}


	public void activateStartGameButton(){

		instructions.gameObject.SetActive(true);	//AH Check this is correct used to be instructions.active
		startGameButton.SetActive(true);

	
	}
	public void startGame(){

		GetComponent<AudioSource>().PlayOneShot(buttonClick);
		animator.speed = 1f;
		animator = GetComponent<Animator> ();
		startGameButton.SetActive(false);
		animator.SetBool ("SlideActivate", false);
		instructions.gameObject.SetActive(false);	//AH Check this is correct used to be instructions.active


		mainScene.GetComponent<Menu_background_selection> ().startGame ();
		Logo.GetComponent<Menu_background_selection> ().fadeOutLogo ();
		skipCutsceneButton.SetActive(true);
//		if (textureSelect == 1){
//
//			Application.LoadLevel ("Comic Framework"); 
//		}
//
//		if (textureSelect == 2){
//			
//			Application.LoadLevel ("Comic Framework"); 
//		}
//
//		if (textureSelect == 3){
//			
//			Application.LoadLevel ("Comic Framework"); 
//		}

	}
}

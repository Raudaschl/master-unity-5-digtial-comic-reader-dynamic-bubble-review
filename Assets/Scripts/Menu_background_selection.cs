﻿using UnityEngine;
using System.Collections;

public class Menu_background_selection : MonoBehaviour {

	public GameObject FadeInOut;

	Animator animator;
	public AudioClip[] otherClip; 
	public bool audioIsPlaying;
	private float comicActivate;
	private float fadeOutActivate;

	void Awake(){
		animator = GetComponent<Animator>();

	}

	void Start () {
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		Resources.UnloadUnusedAssets();

		audioIsPlaying = true;
		fadeOutActivate = 0;
		comicActivate = 0;
		animator.speed = 2f;
	}

	
	void Update (){
		if (GetComponent<AudioSource>().isPlaying) {
			audioIsPlaying = true;
		} else {
			audioIsPlaying = false;
		}

		if (audioIsPlaying == false && comicActivate == 1 && fadeOutActivate == 1){

				Application.LoadLevel ("Comic Framework"); 

		}

		if (audioIsPlaying == false && comicActivate == 2 && fadeOutActivate == 1){
			

			FadeInOut.GetComponent<FadeController> ().FadeOutGame ();
			FadeInOut.GetComponent<FadeController> ().changeScene ();
		 
			
		}
		
	}
	
	// Update is called once per frame
	public void startComic(){
		animator.speed = 1f;
		comicActivate = 1;
		
		animator.SetBool("Comic_select", true);
		GetComponent<AudioSource>().clip = otherClip[0];
		GetComponent<AudioSource>().Play();
	}

	public void startGame(){
		animator.speed = 1f;
		comicActivate = 2;
		
		animator.SetBool("Comic_select", true);
		GetComponent<AudioSource>().clip = otherClip[0];
		GetComponent<AudioSource>().Play();
	}


	public void changeImageSizeActivate(){
	
		animator.SetBool("Game_select", true);

	}

	public void changeImageSizeDeactivate(){
		
		animator.SetBool("Game_select", false);
		
	}


	public void fadeOutLogo(){
		
		animator.SetBool("FadeOut", true);
	}

	public void ActivateFadeOut(){

		fadeOutActivate = 1;
		
		FadeInOut.GetComponent<FadeController> ().FadeOut();
	}

	public void ActivateFadeOutGames(){
		
		fadeOutActivate = 1;
		
		FadeInOut.GetComponent<FadeController> ().FadeOut();
	}
}

﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class downloadComicServer : MonoBehaviour {


	public static AssetBundle bundle;

	public enum cleanCache {No, Yes};
	public cleanCache cleanCacheChoice; 

	public string BundleURL;
	public string comicTitle;
	public int version;

	public float percentDownloaded;

	private WWW www;

	private string[] assetList;

	public string loadingStatus;

	public bool downloadComplete = false;

	public Color fadeOut;

	public GameObject percentageObject;




	// Use this for initialization
	void Start () {


		if (cleanCacheChoice == cleanCache.Yes) {
			cleanAssetBundleCache();
		}
		
		//		Check if comic is cached
		refreshScene ();
	
		

	
	}
	
	// Update is called once per frame
	void Update () {

	

		if (downloadComplete == false) {
			if (www != null) {
				percentDownloaded = www.progress;
				if (percentDownloaded >= 1) {
					downloadComplete = true;
					ES2.Save (true, "medikdizComicDownload.txt?tag=" + BundleURL);
				}
			}
		}
		
		
	}

	public void buttonClick(){

		StartCoroutine (DownloadAndCache());

		ES2.Save (BundleURL, "medikdizComic.txt?tag=comicToLoad");


		if (downloadComplete == true) {

			
			// Frees the memory from the web stream
			www.Dispose();
			
			Debug.Log ("Load Comic");
			Application.LoadLevel("Comic Framework");
		}

	
	}





	IEnumerator DownloadAndCache (){
		
		string BundleURLload = "http://www.medikidz.com/assetBundle/comicpages." + BundleURL;
		// Wait for the Caching system to be ready
		while (!Caching.ready)
			yield return null;
		
		// Load the AssetBundle file from Cache if it exists with the same version or download and store it in the cache
		using (www = WWW.LoadFromCacheOrDownload (BundleURLload, version)) {

			loadingStatus = "loading";
			yield return www;
		

			if (www.error != null) {
				throw new Exception ("WWW download had an error:" + www.error);

			} else {
				loadingStatus = "loaded";
				refreshScene();
		
			}
			
			
		}
	}

	 void loadedAssetBundle(){
		Debug.Log ("Loaded asset" + bundle);
		
		
	}

	string printItems(){
		
		string allPages = "";
		
		for(int i = 0; i < assetList.Length; i++)
		{
			allPages += assetList[i];
		}
		return allPages;
		
	}

	public void cleanAssetBundleCache(){
		Debug.Log ("clean");

		GameObject[] comicThumbnailSelections = GameObject.FindGameObjectsWithTag("comicThumbSelect");

		foreach(GameObject comicThumb in comicThumbnailSelections){

			ES2.Save (false, "medikdizComicDownload.txt?tag=" + comicThumb.GetComponent<downloadComicServer>().BundleURL);

			comicThumb.GetComponent<downloadComicServer>().refreshScene ();
			comicThumb.GetComponent<downloadComicServer>().percentDownloaded = 0;

		}

		Caching.CleanCache ();


	}


	void refreshScene(){

		if (ES2.Exists ("medikdizComicDownload.txt?tag=" + BundleURL)) 
		{
			downloadComplete = ES2.Load<bool>("medikdizComicDownload.txt?tag=" + BundleURL);
			
			if (downloadComplete == false){
				GetComponent<Image>().color = fadeOut;
				www = null;
				if (percentageObject != null){
					percentageObject.SetActive(true);
				}

			} else {

				GetComponent<Image>().color = Color.white;
			}
		}
	}


}

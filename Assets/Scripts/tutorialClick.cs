﻿using UnityEngine;
using System.Collections;

public class tutorialClick : MonoBehaviour {

	private GameObject panelNavigation;

	Animator animator;

	void Start () {
		animator = GetComponent<Animator>();

		panelNavigation = GameObject.Find ("PanelNav");

		panelNavigation.GetComponent<CanvasActivationControl> ().DisableCanvas ();
	}

	public void alterTutorialActivate(){

	
		animator.SetBool("activate", true);


		panelNavigation.GetComponent<CanvasActivationControl> ().EnableCanvas ();

		//load hint system
		if (GameObject.Find ("HintSystem") == null) {
			GameObject HintSystem = (GameObject)Instantiate (Resources.Load ("HintSystem"));
		
			HintSystem.name = HintSystem.name.Replace ("(Clone)", "");
		}

	}


	public void destroy(){

	
		Destroy (gameObject);



	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class hotSpot : MonoBehaviour {

	public HotSpotCard HotSpotCardSelect;
	//Card int which defines type of card to be used - this is pulled from the hotSpotInstantiate script
	public int myCardInt;

	Animator anim;
	int fadeOut = Animator.StringToHash("fadeOut");
	GameObject SubMenu;
	//Navobjects
	private GameObject[] RosterThumbs;
	private GameObject[] hotspotInteractives;
//	private GameObject panelNavigation;

	GameObject RosterCard;

	private GameObject[] numberOfRosterThumbs;

	void Start ()
	{


		GetComponent<hotSpot>().enabled = true;
		SubMenu = GameObject.Find ("SubMenu");
		anim = GetComponent<Animator>();

		//Set up arrays of objects based on TAG
		
//		panelNavigation = GameObject.Find ("PanelNav");


		if (gameObject.CompareTag("RosterThumbs")) {
				
		myCardInt = (int)HotSpotCardSelect;
		}

	}


	public void fadeOutani(){

		//If current gameobject is a hotpot then do the following

		if (gameObject.CompareTag("HotSpot")) {
						anim.SetTrigger (fadeOut);
				}
	
	} 

	public void destroy() {
		Destroy (gameObject);

	}


//Triggers activate roster card for hot spots

	public void OnMouseDown(){

		if (GameObject.Find ("Roster Card") == null) {
			//Play animation to fade out hotspot
			fadeOutani ();
			GetComponent<AudioSource>().Play ();

	
			SubMenu.GetComponent<SlideOut> ().Deactive ();

			//Create Roster Card
			RosterCard = (GameObject)Instantiate (Resources.Load ("Roster Card"));
			RosterCard.name = RosterCard.name.Replace ("(Clone)", "");

			//Activate roster card script
			ActivateRosterCard ();

			//Disable other hotpsots
			GetComponent<hotSpot> ().enabled = false;

		}
	}


	//Activates the Roster Card
	void ActivateRosterCard(){



		//Activate the roster card
//		GameObject RosterCard = GameObject.Find ("Roster Card");
		RosterCard RosterCardScript = RosterCard.GetComponent <RosterCard> ();
		RosterCardScript.myCardInt = myCardInt;
		RosterCardScript.CardStartTrigger ();

		hotspotInteractives = GameObject.FindGameObjectsWithTag ("HotSpot");

//		foreach (GameObject hotspotInteractive in hotspotInteractives) {
//			
//			hotspotInteractive.layer = LayerMask.NameToLayer ("Ignore Raycast");
//		}

		for (int index = 0; index < hotspotInteractives.Length; index++)
		{
			
			var hotspotInteractive = hotspotInteractives[index];
			hotspotInteractive.layer = LayerMask.NameToLayer ("Ignore Raycast");
			
		}

	
		RosterThumbs = GameObject.FindGameObjectsWithTag ("RosterThumbs");

//		foreach (GameObject RosterThumb in RosterThumbs) {
//
////			RosterThumb.GetComponent<Button>().enabled = false;
//
//		}


	}




	//Count number of gameobjects with Roster Thumb TAG
	public void countNumberOfRosterThumbs()
	{
		
		numberOfRosterThumbs = GameObject.FindGameObjectsWithTag("RosterThumbs");
		Debug.Log (numberOfRosterThumbs.Length);
	}




}

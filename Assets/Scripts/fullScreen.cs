﻿using UnityEngine;
using System.Collections;

public class fullScreen : MonoBehaviour {

	void Start(){
		if (Application.platform == RuntimePlatform.OSXWebPlayer
			|| Application.platform == RuntimePlatform.WindowsWebPlayer) {

		} else {
		
			Destroy (gameObject);
		}

	}


	public void toggleFullScreen() {
		
		if (Screen.fullScreen == true) {
			
			Screen.fullScreen = false;
			Screen.SetResolution (960, 640, false);
		} else {
			Screen.fullScreen = true;
			Screen.SetResolution (Screen.currentResolution.width, Screen.currentResolution.height, true);
		}
		
		
	}
}

﻿using UnityEngine;
using System.Collections;

public class buttonClickControl : MonoBehaviour {



	public int pageNum;
	public float PageNumber;
	private GameObject thePageNum;
	private GameObject[] thumbnails;
	private GameObject GlobalAudios;

	private float fadeOutActivate;

	public AudioClip closeSound;


	void Awake(){
		AudioSource audioSource = gameObject.AddComponent<AudioSource>();
		
	}

	// Use this for initialization
	void Start () {



		//Assign audio


		if (gameObject.CompareTag ("thumbnail")) {
			closeSound = Resources.Load ("Audio/UI/Select")as AudioClip;
		} else {
			closeSound = Resources.Load("Audio/UI/Menu-Interface-8-close")as AudioClip;
		}




	}


	void Update () {
//		SelectThumb ();

		if (GlobalAudios == null){

			//Turn off global audio
			GlobalAudios = GameObject.Find("Global Audio");


			if (GlobalAudios == null){
				GlobalAudios = GameObject.Find("Global Audio(Clone)");
			}
		}
	}

//Clicked thumbnail animation play

	public void click() {
		Destroy (GameObject.FindWithTag("HotSpot"));

		//Turn off global audio
		GlobalAudios.GetComponent<globalAudio> ().fadeOutAllGlobalAudio ();

		//Plays selected animation and changes tag to prevent normal thumbnail dissappearing animation triggered by the timeline

		GetComponent<Animation>().Play("selected");
		gameObject.tag = "Selected";


		//Change Page Number of controller on page object
		GameObject Hot = GameObject.Find("Page");
		controller theScript  = Hot.GetComponent<controller>();
		theScript.pageNum = pageNum;
		Hot.GetComponent<controller> ().loadPage ();



	}

	public void clickReturn() {

				//Plays selected animation and changes tag to prevent normal thumbnail dissappearing animation triggered by the timeline

				GetComponent<Animation>().Play ("selected");
				gameObject.tag = "Selected";

		}

	//Build in Animate and move thumbnails after selecting one
	public void appear() {
		
		GameObject[] thumbnails = GameObject.FindGameObjectsWithTag("thumbnail");
		
//		foreach (GameObject thumbnail in thumbnails) {
//			thumbnail.GetComponent<Animation>().Play("appear");
//
//		}

		for (int index = 0; index < thumbnails.Length; index++)
		{
			
			var thumbnail = thumbnails[index];
			thumbnail.GetComponent<Animation>().Play("appear");
			
		}
		
	}

	//Animate and move thumbnails after selecting one
	public void disappear() {

		GameObject[] thumbnails = GameObject.FindGameObjectsWithTag("thumbnail");

//		foreach (GameObject thumbnail in thumbnails) {
//			thumbnail.GetComponent<Animation>().Play("disappear");
//			gameObject.tag = "thumbnail";
//		
//		}

		for (int index = 0; index < thumbnails.Length; index++)
		{
			
			var thumbnail = thumbnails[index];
			thumbnail.GetComponent<Animation>().Play("disappear");
			gameObject.tag = "thumbnail";
			
		}

		GameObject Hot = GameObject.Find("Page Menu");
		Hot.GetComponent<SlideOut>().TriggerOut();

		closeSoundAudio ();

}

	//Animate and move thumbnails after selecting one
	public void disappearSettings() {
		
		closeSoundAudio ();
		GameObject Hot = GameObject.Find("Settings");
		Hot.GetComponent<SlideOut>().TriggerOut();

	}

	//Move our credits menu
	public void disappearCredits() {
		
		closeSoundAudio ();
		GameObject Hot = GameObject.Find("Credits");
		Hot.GetComponent<SlideOut>().TriggerOut();
		
	}

	//Move our share sub-menu
	public void disappearShareSubMenu() {
		
		closeSoundAudio ();
		GameObject Hot = GameObject.Find("Share SubMenu");
		Hot.GetComponent<SlideOut>().TriggerOut();
		
	}

	
	//Move our credits menuexit popup
	public void disappearExitPopup() {
		
		closeSoundAudio ();
		GameObject Hot = GameObject.Find("Exit Popup");
		Hot.GetComponent<SlideOut>().TriggerOut();
		
	}

	//Animate and move thumbnails after selecting one
	public void disappearInstructions() {
		
		closeSoundAudio ();
		GameObject Hot = GameObject.Find("Instructions");
		Hot.GetComponent<SlideOut>().InstructionsTriggerOut();
		

		
	}

	public void closeSoundAudio(){
	
		GetComponent<AudioSource>().PlayOneShot(closeSound);
	}

	public void SelectThumb(){
		//Find page number and highlight thumbnail
		thumbnails = GameObject.FindGameObjectsWithTag("thumbnail");

		thePageNum = GameObject.Find("Page");
		
		controller playerScript = thePageNum.GetComponent<controller>();
		PageNumber = playerScript.pageNum;

		//Highlight thumb of current page
//		foreach (GameObject thumbnail in thumbnails) {
//			
//			if(thumbnail.name == "page" + PageNumber)
//				thumbnail.transform.localScale = new Vector3(0.85F, .85F, .85F);
//
//		}

		for (int index = 0; index < thumbnails.Length; index++)
		{
			
			var thumbnail = thumbnails[index];
			if(thumbnail.name == "page" + PageNumber)
				thumbnail.transform.localScale = new Vector3(0.85F, .85F, .85F);
			
		}







	}

	//Return to main menu

	public void returnToMainMenu(){

		GameObject FadeInOut = GameObject.FindWithTag("FadeInOut");
		fadeOutActivate = 1;
		
		FadeInOut.GetComponent<FadeController> ().FadeOutMainMenu();
	}



}
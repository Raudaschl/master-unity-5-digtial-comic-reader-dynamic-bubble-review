﻿using UnityEngine;
using System.Collections;

public class audioPlay : loading_prefab {

	public AudioClip soundEffect1;
	public AudioClip soundEffect2;
	public AudioClip soundEffect3;
	public AudioClip soundEffect4;
	public AudioClip soundEffect5;
	public AudioClip soundEffect6;

	private GameObject background1;
	private AudioSource background1AudioSource;
	private GameObject background2;
	private AudioSource background2AudioSource;
	private GameObject background3;
	private AudioSource background3AudioSource;


	[Range(0.1f, 2.0f)]
	public float background1FadeoutTime = 0.5f;

	[Range(0.1f, 2.0f)]
	public float background2FadeoutTime = 0.5f;

	[Range(0.1f, 2.0f)]
	public float background3FadeoutTime = 0.5f;


	private globalAudio globalAudioComponent;
	private AudioSource audioSource;

	
	//Background1
	private float background1_vol = 1.0f;
	private int background1_fadeout_active = 0;

	//Background2
	private float background2_vol = 1.0f;
	private int background2_fadeout_active = 0;

	//Background3
	private float background3_vol = 1.0f;
	private int background3_fadeout_active = 0;


	void Awake(){
		audioSource = gameObject.AddComponent<AudioSource>();

	}


	void Start() {


		localAudioSetup ();


		
		
		
//		Global Audio
		if (globalAudio == null) {
		
			if (globalAudioCheck() == true) {
			
				if (globalAudioComponent == null) {
					globalAudioComponent = globalAudio.GetComponent<globalAudio> ();
//					Debug.Log ("Component Assigned");
				} else {
					Debug.Log ("globalAudioComponent already assigned");
				}
				
			} else {
				Debug.Log ("ERROR! No Global Audio Object in scene");

			}
		}

	}

	//Check if global audio object has been assigned
	public bool globalAudioCheck(){

			if (GameObject.Find ("Global Audio(Clone)")) {
				globalAudio = GameObject.Find ("Global Audio(Clone)");
			Debug.Log ("Global Audio Assigned");
				return true;
			} else if (GameObject.Find ("Global Audio")) {
				globalAudio = GameObject.Find ("Global Audio");
			Debug.Log ("Global Audio Assigned2");
				return true;
			} else {
				Debug.Log ("ERROR! No Global Audio Object Found");
				return false;
			}

	}

	void Update(){



		if (background1 == null){
	
			localAudioSetup ();

			background1_fadeout_active = 0;
			background2_fadeout_active = 0;
			background3_fadeout_active = 0;
		}

		//Handle background1 fade out


		background1AudioSource.volume = background1_vol;
		if (background1AudioSource.volume > 0 && background1_fadeout_active == 1) {
			background1_vol_fadeout ();
				} 
		if (background1AudioSource.volume <= 0) {
			background1_fadeout_active = 0;
			background1AudioSource.Stop();
		}

		//Handle background2 fade out
		background2AudioSource.volume = background2_vol;
		if (background2AudioSource.volume > 0 && background2_fadeout_active == 1) {
			background2_vol_fadeout ();
//			Debug.Log ("Fade Out Active");
		} 
		if (background2AudioSource.volume <= 0 && background2_fadeout_active == 1) {
			background2AudioSource.Stop();
			background2_fadeout_active = 0;
//			Debug.Log ("Fade Out Stop");
		}


		if (background3 != null) {
			//Handle background3 fade out
			background3AudioSource.volume = background3_vol;
			if (background3AudioSource.volume > 0 && background3_fadeout_active == 1) {
				background3_vol_fadeout ();
			} 
			if (background3AudioSource.volume <= 0) {
				background3_fadeout_active = 0;
				background3AudioSource.Stop ();
			}
		}

	}

	public void localAudioSetup(){
		
		background1 = GameObject.Find("background1");
		background1AudioSource = background1.GetComponent<AudioSource> ();
		
		background2 = GameObject.Find("background2");
		background2AudioSource = background2.GetComponent<AudioSource> ();
		
		background3 = GameObject.Find("background3");
		if (background3 != null){
		background3AudioSource = background3.GetComponent<AudioSource> ();
		}
		
		
	}

	//Sound Effect Controllers
	public void soundEffect1_audio() {
		audioSource.PlayOneShot(soundEffect1);

	}

	public void soundEffect2_audio() {
		audioSource.PlayOneShot(soundEffect2);
	}

	public void soundEffect3_audio() {
		audioSource.PlayOneShot(soundEffect3);
	}

	public void soundEffect4_audio() {
		audioSource.PlayOneShot(soundEffect4);
	}

	public void soundEffect5_audio() {
		audioSource.PlayOneShot(soundEffect5);
	}

	public void soundEffect6_audio() {
		audioSource.PlayOneShot(soundEffect6);
	}

	//Background Music Effects

	public void background1_audio() {

		if (Direction == 2) {

			background1AudioSource.Play ();
			background1_vol = 1.0f;
			background1_fadeout_active = 0;
		} else {
		
			background1_fadeout();
		}



	}

	public void background2_audio() {

		if (Direction == 2) {
			background2AudioSource.Play ();
			background2_vol = 1.0f;
			background2_fadeout_active = 0;
		}else {
			
			background2_fadeout();
		}

	}

	public void background3_audio() {
		if (Direction == 2) {

			background3AudioSource.Play ();
			background3_vol = 1.0f;
		
		} else {
		
			background3_fadeout ();
		}
	}

	//Background Fade out function

	public void background1_fadeout(){


			background1_fadeout_active = 1;
			

	}

	public void background2_fadeout(){

			background2_fadeout_active = 1;

	}


	public void background3_fadeout(){
			background3_fadeout_active = 1;


	}


	//Audio fadeout function

	private void background1_vol_fadeout(){
		background1_vol = background1_vol - (background1FadeoutTime*Time.deltaTime);
	}

	private void background2_vol_fadeout(){
		background2_vol = background2_vol - (background2FadeoutTime*Time.deltaTime);
	}

	private void background3_vol_fadeout(){
		background3_vol = background3_vol - (background3FadeoutTime*Time.deltaTime);
	}

	//GLOBAL AUDIO CONTROLLER

	//Play Global audio 1
	public void playGlobalBackgroundAudio1() {

		if (globalAudioComponent != null){
			globalAudioComponent.globalBackgroundAudio_audio(0);
		}
	}

	//Play Global audio 2
	public void playGlobalBackgroundAudio2() {
		if (globalAudioComponent != null){
			globalAudioComponent.globalBackgroundAudio_audio(1);
		}
	}
	//Play Global audio 3
	public void playGlobalBackgroundAudio3() {
		if (globalAudioComponent != null){
			globalAudioComponent.globalBackgroundAudio_audio(2);
		}
	}

	//Play Global audio 4
	public void playGlobalBackgroundAudio4() {
		if (globalAudioComponent != null){
			globalAudioComponent.globalBackgroundAudio_audio(3);
		}
	}

	//Play Global audio 5
	public void playGlobalBackgroundAudio5() {
		if (globalAudioComponent != null){
			globalAudioComponent.globalBackgroundAudio_audio(4);
		}
	}

	//Play Global audio 6
	public void playGlobalBackgroundAudio6() {
		if (globalAudioComponent != null){
			globalAudioComponent.globalBackgroundAudio_audio(5);
		}
	}

	//Play Global audio 7
	public void playGlobalBackgroundAudio7() {
		if (globalAudioComponent != null){
			globalAudioComponent.globalBackgroundAudio_audio(6);
		}
	}

	//Play Global audio 8
	public void playGlobalBackgroundAudio8() {
		if (globalAudioComponent != null){
			globalAudioComponent.globalBackgroundAudio_audio(7);
		}
	}


	//Play Global audio 9
	public void playGlobalBackgroundAudio9() {
		if (globalAudioComponent != null){
			globalAudioComponent.globalBackgroundAudio_audio(8);
		}
	}

	//Play Global audio 10
	public void playGlobalBackgroundAudio10() {
		if (globalAudioComponent != null){
			globalAudioComponent.globalBackgroundAudio_audio(9);
		}
	}










	//STOP Global audio 1

	public void stopGlobalBackgroundAudio1() {
		if (globalAudioComponent != null){
			StartCoroutine(globalAudioComponent.globalBackgroundAudio_fadeout(0));
		}
	}
	
	//STOP Global audio 2
	public void stopGlobalBackgroundAudio2() {
		if (globalAudioComponent != null){
			StartCoroutine(globalAudioComponent.globalBackgroundAudio_fadeout(1));
		}
	}

	//STOP Global audio 3
	public void stopGlobalBackgroundAudio3() {
		if (globalAudioComponent != null){
			StartCoroutine(globalAudioComponent.globalBackgroundAudio_fadeout(2));
		}
	}

	//STOP Global audio 4
	public void stopGlobalBackgroundAudio4() {
		if (globalAudioComponent != null){
			StartCoroutine(globalAudioComponent.globalBackgroundAudio_fadeout(3));
		}
	}

	//STOP Global audio 5
	public void stopGlobalBackgroundAudio5() {
		if (globalAudioComponent != null){
			StartCoroutine(globalAudioComponent.globalBackgroundAudio_fadeout(4));
		}
	}

	//STOP Global audio 6
	public void stopGlobalBackgroundAudio6() {
		if (globalAudioComponent != null){
			StartCoroutine(globalAudioComponent.globalBackgroundAudio_fadeout(5));
		}
	}

	//STOP Global audio 7
	public void stopGlobalBackgroundAudio7() {
		if (globalAudioComponent != null){
			StartCoroutine(globalAudioComponent.globalBackgroundAudio_fadeout(6));
		}
	}

	//STOP Global audio 8
	public void stopGlobalBackgroundAudio8() {
		if (globalAudioComponent != null){
			StartCoroutine(globalAudioComponent.globalBackgroundAudio_fadeout(7));
		}
	}

	//STOP Global audio 9
	public void stopGlobalBackgroundAudio9() {
		if (globalAudioComponent != null){
			StartCoroutine(globalAudioComponent.globalBackgroundAudio_fadeout(8));
		}
	}

	//STOP Global audio 10
	public void stopGlobalBackgroundAudio10() {
		if (globalAudioComponent != null){
			StartCoroutine(globalAudioComponent.globalBackgroundAudio_fadeout(9));
		}
	}


}

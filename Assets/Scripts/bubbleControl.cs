using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;
using TMPro;

[ExecuteInEditMode]
public class bubbleControl : languageControl {

	public GameObject languageManager;

	public TextMeshPro bubbleText;

	public TextAsset scoreDataTextFile;

	public XmlNode dialogue;

	public int pageNum = 1;

	public int bubbleNum = 1;



	public GameObject bubbleObject;
	private Transform bubbleTransform;


	public string originalScale;

	private languageManager languageManagerComponent;



	void Awake(){

		languageManager = GameObject.Find ("Language Manager");
		languageManagerComponent = languageManager.GetComponent<languageManager>();

		myName = languageManagerComponent.myName;
	}

	void Start() {

		language = languageManagerComponent.language;


		//Set Language

		languageSwitch (language.ToString());
		bubbleTransform = bubbleObject.transform;






	}

	void Update() {



		if (language.ToString() != prevLanguagetSelect) {
		
			languageSwitch (language.ToString());
		}



	
	}

	void bubbleSize(string scale){
		string[] scaleArr;

		scale = scale.Replace ("(", "").Replace(")", "");
		scaleArr = scale.Split (","[0]);
		
		
		
		float x = float.Parse(scaleArr[0]);
		float y = float.Parse(scaleArr[1]);
		float z = float.Parse(scaleArr[2]);
		
		bubbleObject.transform.localScale = new Vector3(x,y,z);
	}

	//Set Language
	void languageSwitch(string language) {
		prevLanguagetSelect = language.ToLower ();
		switch (prevLanguagetSelect) {
		case "english":
			langSelectXml = prevLanguagetSelect;
			fontSwitch("latin");
			break;
		case "french":
			langSelectXml = prevLanguagetSelect;
			fontSwitch("latin");
			break;
		case "german":
			langSelectXml = prevLanguagetSelect;
			fontSwitch("latin");
			break;
		case "italian":
			langSelectXml = prevLanguagetSelect;
			fontSwitch("latin");
			break;
		case "spanish":
			langSelectXml = prevLanguagetSelect;
			fontSwitch("latin");
			break;
		case "arabic":
			langSelectXml = prevLanguagetSelect;
			fontSwitch("arabic");
			break;
		case "japanese":
			langSelectXml = prevLanguagetSelect;
			fontSwitch("japanese");
			break;
		default:
			Debug.Log ("Uh oh something is wrong with language select!");
			break;
		}
		assignBubbles();
	}

	private void assignBubbles(){
		string textData = scoreDataTextFile.text;
		ParseScoreXML( textData );
	
	}
	
	private void ParseScoreXML(string xmlData) {
		XmlDocument xmlDoc = new XmlDocument();
		xmlDoc.Load( new StringReader(xmlData) );
		string xmlPathPattern = "/comicPages/page" + pageNum.ToString() + "/bubble" + bubbleNum.ToString() + "/" +langSelectXml;
		XmlNodeList myNodeList = xmlDoc.SelectNodes(xmlPathPattern );


		foreach (XmlNode node in myNodeList) {
			BubbleRecordString (node);
		}




//		//Single Element Load
//		Debug.Log (myNodeList.Item(1).InnerXml);

//		
//		XmlElement root = xmlDoc.DocumentElement;
//
//		XmlNodeList nodeList = root.SelectNodes(xmlPathPattern);
//		Debug.Log(nodeList.Item(0).InnerXml); 

	}


	//Loop Load all nodes
	private void BubbleRecordString (XmlNode node) {



		XmlNode bubbleTextNode = node.FirstChild;
		XmlNode alignNode = bubbleTextNode.NextSibling;
		XmlNode sizeNode = alignNode.NextSibling;
		XmlNode fontNode = sizeNode.NextSibling;

//		Debug.Log ("text = " + bubbleTextNode.InnerXml + " alignment = " + alignNode.InnerXml + " size = " + sizeNode.InnerXml + " font = " + fontNode.InnerXml);

		//Assign Bubble Text Attributes
		string parseBubbleText = bubbleTextNode.InnerXml;

		//Set name for *name* tag
		if (myName == ""){
			myName = "Jim";
		}

		parseBubbleText = parseBubbleText.Replace("*name*", myName).Replace("{", "<").Replace("}", ">");
		bubbleText.text = parseBubbleText;

		//Set Bubble Size
		if (sizeNode.InnerXml != "") {
			bubbleSize (sizeNode.InnerXml);
		} else {
			if (originalScale != ""){
			bubbleSize(originalScale);
			}
		}

		//Set Text Alignment
		alignmentSwitch (alignNode.InnerXml);

		//Set Font (usually changed for language)
//		fontSwitch (fontNode.InnerXml);

		//Set line space

		
	}


	//Set Alignment
	void alignmentSwitch(string alignment) {
		alignment = alignment.ToLower();
		switch (alignment) {
		case "left":
			bubbleText.alignment = TextAlignmentOptions.Left;
			break;
		case "right":
			bubbleText.alignment = TextAlignmentOptions.Right;
			break;
		case "center":
			bubbleText.alignment = TextAlignmentOptions.Center;
			break;
		default:
//			bubbleText.alignment = TextAlignmentOptions.Center;
			break;
		}

	}

	//Set font
	void fontSwitch(string font) {
		font = font.ToLower ();
		switch (font) {
		case "latin":
			bubbleText.font = languageManagerComponent.fontSelect[0];
			alignmentSwitch("center");
			bubbleText.alignment = TextAlignmentOptions.Midline;
			bubbleText.fontSizeMin = 12;
			break;
		case "arabic":
			bubbleText.font = languageManagerComponent.fontSelect[1];
			alignmentSwitch("right");
			bubbleText.alignment = TextAlignmentOptions.Midline;
			bubbleText.fontSizeMin = 12;
			break;
		case "japanese":
			bubbleText.font = languageManagerComponent.fontSelect[2];
			bubbleText.alignment = TextAlignmentOptions.MidlineJustified;
			bubbleText.fontSizeMin = 20  ;
			break;
		default:
			bubbleText.font = languageManagerComponent.fontSelect[0];
			bubbleText.fontSizeMin = 12;
			break;
		}

	}

	public void WriteToXml()
	{
		
		string filepath = Application.dataPath + @"/0.xml";

		XmlDocument xmlDoc = new XmlDocument();
		
		if(File.Exists (filepath))
		{
			xmlDoc.Load(filepath);
			
			XmlElement elmRoot = xmlDoc.DocumentElement;

			
			elmRoot.RemoveAll(); // remove all inside the transforms node.

			XmlElement elmNew1 = xmlDoc.CreateElement("page" + pageNum.ToString()); // create the rotation node.
			
			XmlElement elmNew2 = xmlDoc.CreateElement("bubble" + bubbleNum.ToString()); // create the rotation node.

			XmlElement elmNew3 = xmlDoc.CreateElement(langSelectXml); // create the rotation node.
			
			XmlElement rotation_X = xmlDoc.CreateElement("textBub"); // create the x node.
			rotation_X.InnerText = "1x"; // apply to the node text the values of the variable.
			
			XmlElement rotation_Y = xmlDoc.CreateElement("alignment"); // create the y node.
			rotation_Y.InnerText = "1y"; // apply to the node text the values of the variable.
			
			XmlElement rotation_Z = xmlDoc.CreateElement("size"); // create the z node.
			rotation_Z.InnerText = "1z"; // apply to the node text the values of the variable.
			
			elmNew3.AppendChild(rotation_X); // make the rotation node the parent.
			elmNew3.AppendChild(rotation_Y); // make the rotation node the parent.
			elmNew3.AppendChild(rotation_Z); // make the rotation node the parent.
			elmNew2.AppendChild(elmNew3); // make the transform node the parent.
			elmNew1.AppendChild(elmNew2); // make the transform node the parent
			elmRoot.AppendChild(elmNew1); // make the transform node the parent
			
			xmlDoc.Save(filepath); // save file.
		}
	}

	
	
	
}


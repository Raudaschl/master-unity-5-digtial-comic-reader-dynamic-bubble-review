﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class disableIfNotLoaded : MonoBehaviour {

	// Use this for initialization
		public GameObject loader;
	public Text TextObject;

	public float percentageLoaded;
		
		
		// Update is called once per frame
		void Update () {


		percentageLoaded = loader.GetComponent<streamingTest> ().percentageLoaded;

			if (percentageLoaded == 100) {

			GetComponent<Button>().interactable = true;
				gameObject.SetActive(true);
			if (TextObject != null){
				TextObject.text = "";
			}

			} else {
			GetComponent<Button>().interactable = false;

			if (TextObject != null){
				TextObject.text = "LOADING " + percentageLoaded +"%";
			}
		}
	}

}

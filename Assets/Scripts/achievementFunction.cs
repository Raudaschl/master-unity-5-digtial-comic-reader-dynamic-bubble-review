﻿using UnityEngine;
using System.Collections;

public class achievementFunction : MonoBehaviour {
	public AudioClip achievement;


	void Awake() {
		gameObject.AddComponent<AudioListener>();
	}
	// Use this for initialization
	void Start () {

		GetComponent<AudioSource>().PlayOneShot(achievement);
	
	}
	

}

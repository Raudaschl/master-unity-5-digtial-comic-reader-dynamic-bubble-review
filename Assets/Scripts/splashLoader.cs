﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class splashLoader : MonoBehaviour {

	// Use this for initialization
	public GameObject loader;
	public Text TextObject;
	public GameObject SkipButton;
	public float percentageLoaded;
	
	
	// Update is called once per frame
	void Update () {
		
		
		percentageLoaded = loader.GetComponent<streamingTest> ().percentageLoaded;
		
		if (percentageLoaded == 100) {
			
		
			gameObject.SetActive(true);
			if (TextObject != null){
				TextObject.text = "";
				SkipButton.SetActive(true);
			}
			
		} else {
			GetComponent<Button>().interactable = false;
			
			if (TextObject != null){
				TextObject.text = "LOADING " + Mathf.Round(percentageLoaded) +"%";
			}
		}
	}
}

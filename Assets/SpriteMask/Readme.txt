----------------------------------------------
                 SpriteMask
          Copyright � 2015 TrueSoft
             support@truesoft.pl
			    Version: 1.0
----------------------------------------------


 HELP
----------------------------------------------

1) How to create mask in Unity editor?

	Just add component SpriteMask to GameObject.

2) Is it possible to create mask at runtime?

	Yes of course, just look at scene "Examples\Scenes\05 - Runtime mask.unity".
	
3) Can I use my own shader?

	Yes, it is possible, but you should ensure that your shader support stencil buffer. 
	In your shader source code you should have this:

	Properties
	{
		// [...]
	
		_Stencil ("Stencil Ref", Float) = 0
		_StencilComp ("Stencil Comparison", Float) = 8
	}
	
	SubShader
	{
		Pass
		{
			Stencil
			{
				Ref [_Stencil]
				Comp [_StencilComp]
				Pass Keep
			}
			
			// [...]
		}
	}
	
	For more info see source code of shader "SpriteMask/Default" in file "Resources\SpriteDefault.shader".

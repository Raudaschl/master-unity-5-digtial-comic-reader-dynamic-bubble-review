﻿using UnityEngine;
using System.Collections;

public class menuImageSwap : MonoBehaviour {

	public Texture[] Textures;
	public int textureSelect;
	public GameObject sceneObject;

	protected Animator animator;
	// Use this for initialization
	void Start () {

		textureSelect = 0;

	}
	
	// Update is called once per frame
	void Update () {

		if (gameObject.transform.name == "COMIC")
		GetComponent<Renderer>().material.mainTexture = Textures[textureSelect];

	}




	public void imageComic(){

		sceneObject.GetComponent<Menu_background_selection> ().changeImageSizeDeactivate ();
		textureSelect = 0;
	}

	public void imageSpotProtein(){


		sceneObject.GetComponent<Menu_background_selection> ().changeImageSizeActivate ();
		textureSelect = 1;
	}

	public void imageProteinDodge(){
		sceneObject.GetComponent<Menu_background_selection> ().changeImageSizeActivate ();
		textureSelect = 2;
	}

	public void imageJigsaw(){
		sceneObject.GetComponent<Menu_background_selection> ().changeImageSizeActivate ();
		textureSelect = 3;
	}
}

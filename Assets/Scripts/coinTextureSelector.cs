﻿using UnityEngine;
using System.Collections;

public class coinTextureSelector : MonoBehaviour {


	public Sprite[] item;



	private SpriteRenderer spriteRenderer;
	private int randomNumber;


	// Use this for initialization
	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer>();
		randomNumber = Random.Range (1, 10);

		spriteRenderer.sprite = item[randomNumber];
	}
	

}

﻿using UnityEngine;
using System.Collections;
using TMPro;

[ExecuteInEditMode]
public class languageControl : MonoBehaviour {



	public enum Language{
		english,
		french,
		german,
		italian,
		spanish,
		arabic,
		japanese
	}
	
	public TextMeshProFont[] fontSelect;
	
	public Language language;
	[HideInInspector]
	public static string prevLanguagetSelect;
	[HideInInspector]
	public string langSelectXml;
	
	public string myName;



}

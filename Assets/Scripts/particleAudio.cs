﻿using UnityEngine;
using System.Collections;

public class particleAudio : MonoBehaviour {


	private float mySliderFloat;
	
	public AudioClip particleSoundEffect;
	public int rateChangeMarker;
	public int intervalTrigger;
	public float audioDelay;
	[Range(0.0f, 1.0f)]
	public float vol = 1.0f;
	private bool soundTrigger;
	
	// Use this for initialization
	void Start () {
		AudioSource audioSource = gameObject.AddComponent<AudioSource>();

	}
	
	// Update is called once per frame
	void Update () {


		GetComponent<AudioSource>().volume = vol;

		rateChangeMarker = GetComponent<ParticleSystem>().particleCount;

		if (GetComponent<ParticleSystem>().particleCount == intervalTrigger && soundTrigger == true){
			soundTrigger = false;
			StartCoroutine(audioPlayer());

		}

		if (GetComponent<ParticleSystem>().particleCount != intervalTrigger){
			soundTrigger = true;

		}

		}

	IEnumerator audioPlayer() {


		yield return new WaitForSeconds(audioDelay);
		if (!GetComponent<AudioSource>().isPlaying) {
		GetComponent<AudioSource>().PlayOneShot(particleSoundEffect);
		}
	}
}
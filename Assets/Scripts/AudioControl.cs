﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AudioControl : MonoBehaviour {

	float RecordAudioLevel;
	public Button clickButton;
	public Sprite MuteOn;
	public Sprite MuteOff;
	// Use this for initialization


	//When slider changes update volume level at audio listener
	public void audioLevelLog(){
		
		Slider AudioSlider = gameObject.GetComponent<Slider>();

		GameObject Audio = GameObject.Find ("Audio");

		AudioListener Controller = Audio.GetComponent<AudioListener>();
		
		AudioListener.volume = AudioSlider.value;

	


		//Control Mute texture
		if (AudioSlider.value != 0f) {
						Image MuteImage = clickButton.GetComponent<Image> ();
						MuteImage.sprite = MuteOff;
				} else {
			Image MuteImage = clickButton.GetComponent<Image> ();
			MuteImage.sprite = MuteOn;
		}

	}


	//Mute when button pressed
	public void Mute(){


		//Store Value
		Slider AudioSlider = gameObject.GetComponent<Slider>();

		Image MuteImage = clickButton.GetComponent<Image>();

		if (AudioSlider.value != 0f) {

			//Store Value
			RecordAudioLevel = AudioSlider.value;

			//Mute Texture
			MuteImage.sprite = MuteOn;

			//Mute Audio
						AudioSlider.value = 0f;

				} else {
			//Reset Audio to previous level
			AudioSlider.value = RecordAudioLevel;
			MuteImage.sprite = MuteOff;
		}
		
	}

}

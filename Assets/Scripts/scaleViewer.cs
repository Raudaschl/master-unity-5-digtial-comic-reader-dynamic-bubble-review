﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class scaleViewer : MonoBehaviour {

	public string scaleView;
	public string currentLanguage;
	// Update is called once per frame
	void Update () {

		currentLanguage = bubbleControl.prevLanguagetSelect.ToString();
		scaleView = gameObject.transform.localScale.ToString();
	}
}

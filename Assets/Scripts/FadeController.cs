﻿using UnityEngine;
using System.Collections;

public class FadeController : MonoBehaviour {

	protected Animator animator;
	private string sceneToLoad;

	public string[] LevelName; 
	
	public int gameSelect;

	void Awake(){
		animator = GetComponent<Animator> ();
		
	}

	// Use this for initialization
	void Start () {


		animator.speed = 0;
		animator.Play("PanelFadeOut", -1, 0f);


	
	}

	
	public void FadeOut(){
	
		animator.speed = 1;


	}

	public void FadeOutMainMenu(){
	
		animator.speed = 1;
		sceneToLoad = "Main Menu";


	}

	public void FadeOutComic(){
		
		animator.speed = 1;
		sceneToLoad = "Comic Framework";
		
		
	}

	public void FadeOutGame(){
		
		animator.speed = 1;
		sceneToLoad = LevelName[gameSelect];
		
		
	}

	public void changeScene(){

		if (sceneToLoad != null) {
			Application.LoadLevel (sceneToLoad); 
		}
		
	}
}

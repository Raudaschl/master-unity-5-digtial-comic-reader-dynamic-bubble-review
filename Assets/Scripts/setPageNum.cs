﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class setPageNum : MonoBehaviour {

	public int PageNumber;
	private GameObject thePageNum;

	private Text instruction;
	
	// Use this for initialization
	void Start () {

		instruction = gameObject.GetComponent<Text>();

	}
	


	public void setPageNumText(){

		thePageNum = GameObject.Find("Page");
		controller playerScript = thePageNum.GetComponent<controller>();
		PageNumber = playerScript.pageNum;

		instruction.text = "PAGE " + PageNumber +"/" + loading_prefab.pageCount;
	}
}

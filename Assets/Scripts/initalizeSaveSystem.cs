﻿using UnityEngine;
using System.Collections;

public class initalizeSaveSystem : MonoBehaviour {

	int thumbCounter;
	int firstOpened;

	public int totalNumberOfCards;

	public int totalActivatedCards;

	private GameObject[] numberOfRosterThumbs;



	// Use this for initialization
	void Start () {
		thumbCounter = 1;

		if (ES2.Exists ("medikdizComic.txt?tag=firstOpened"))
		{
			if (ES2.Load<int>("medikdizComic.txt?tag=firstOpened") == 0) 
			{
				Debug.Log("Activated but disabled");
			} else {
			
				Debug.Log("Previously activated and opened");
			}
		} else {
			Debug.Log("Never been activated");

			ES2.Save (1, "medikdizComic.txt?tag=firstOpened");
			StartCoroutine("debugTurnCardsOff", 0.2f);

			StartCoroutine("tutorialShow", 0.2f);
		
		}

		if (ES2.Exists ("medikdizComic.txt?tag=pageNumber")) 
		{
			Debug.Log(ES2.Load<int>("medikdizComic.txt?tag=pageNumber"));
		}

	}
	
	// Update is called once per frame
	void LateUpdate () {

		//DEBUG TURN ON CARDS THUMBS
		if (Input.GetKeyDown ("space")) {
			StartCoroutine("debugTurnCardsOn", 0.2f);
		}

		if (Input.GetKeyDown ("d")) {
			StartCoroutine("debugTurnCardsOff", 0.2f);
		}

		if (Input.GetKeyDown ("c")) {
			StartCoroutine("debugCheckCards", 0.2f);
		}

		if (Input.GetKeyDown ("x")) {
			StartCoroutine("debugDeleteCards", 0.2f);
		}

		if (Input.GetKeyDown ("a")) {
						checkActivatedCards ();
				}

		if (thumbCounter >= 9) {
			thumbCounter = 1;

		}

		if (Input.GetKeyDown ("1")) {

			//Camera and Navigation Prefabs
			GameObject ProgressBar = (GameObject)Instantiate(Resources.Load("Progress Bar"));
			
			ProgressBar.name = ProgressBar.name.Replace("(Clone)", "");
		}

		
	}
	




	//DEBUG TURN ON CARDS THUMBS
	IEnumerator debugTurnCardsOn(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		numberOfRosterThumbs = GameObject.FindGameObjectsWithTag("RosterThumbs");
		
		
		
		while (thumbCounter <= numberOfRosterThumbs.Length){
			
			ES2.Save (1, "medikdizComic.txt?tag=Card" + thumbCounter);
			Debug.Log(thumbCounter);

			
			thumbCounter++;
			
		}
		
		
		
		
	}

	//DEBUG TURN OFF CARDS THUMBS
	IEnumerator debugTurnCardsOff(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		numberOfRosterThumbs = GameObject.FindGameObjectsWithTag("RosterThumbs");
		
		
		
		while (thumbCounter <= numberOfRosterThumbs.Length){
			
			ES2.Save (0, "medikdizComic.txt?tag=Card" + thumbCounter);
			Debug.Log(thumbCounter);
			
			
			thumbCounter++;
			
		}
		
		
		
		
	}

	//DEBUG CHECK VALUE OF CARD ACTIVATION
	IEnumerator debugCheckCards(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		numberOfRosterThumbs = GameObject.FindGameObjectsWithTag("RosterThumbs");
		
		
		
		while (thumbCounter <= numberOfRosterThumbs.Length){
			
		
			Debug.Log(ES2.Load<int>("medikdizComic.txt?tag=Card"+(thumbCounter)));
			
			
			thumbCounter++;
			
		}
		
		
		
		
	}

	//DEBUG DELETE ALL CARD VALUES
	IEnumerator debugDeleteCards(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		numberOfRosterThumbs = GameObject.FindGameObjectsWithTag("RosterThumbs");
		
		
		
		while (thumbCounter <= numberOfRosterThumbs.Length){
			
			
			ES2.Delete("medikdizComic.txt?tag=Card"+(thumbCounter));
			Debug.Log("Deleted");
			
			thumbCounter++;
			
		}

		ES2.Delete ("medikdizComic.txt?tag=firstOpened");
	}

	//Check number of activated cards
	public void checkActivatedCards()
	{
		totalActivatedCards = 0;

		numberOfRosterThumbs = GameObject.FindGameObjectsWithTag("RosterThumbs");

		while (thumbCounter <= numberOfRosterThumbs.Length){
			
			if (ES2.Exists ("medikdizComic.txt?tag=Card"+thumbCounter)) 
			{
				if (ES2.Load<int>("medikdizComic.txt?tag=Card"+thumbCounter) == 1) 
				{
					totalActivatedCards++;
				}
			}


			//What is the total number of activated cards?
			if (thumbCounter >= totalNumberOfCards){
//				Debug.Log(totalActivatedCards);
			}


			
			thumbCounter++;
			
		}
	}

	IEnumerator tutorialShow(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		//Tutorial
		GameObject Tutorial = (GameObject)Instantiate(Resources.Load("Tutorial"));
		
		Tutorial.name = Tutorial.name.Replace("(Clone)", "");
		

	}
}

﻿using UnityEngine;
using System.Collections;

public class menuButtonControl : MonoBehaviour {

	public GameObject BackgroundScene;
	public GameObject Logo;

	public GameObject gameMenu;

	protected Animator animator;
	

	public AudioClip buttonClick;


	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		animator.speed = 3f;
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void slideOutButton() {

		animator.SetBool ("Slide_Out", true);
		GetComponent<AudioSource>().PlayOneShot(buttonClick);

	}

	public void slideInButton() {

		animator.SetBool ("Slide_Out", false);
		
		
	}

	public void startComic() {

		animator.speed = 1f;
		BackgroundScene.GetComponent<Menu_background_selection> ().startComic ();
		
		Logo.GetComponent<Menu_background_selection> ().fadeOutLogo ();
	}

	public void gameMenuStart() {
		
		animator.speed = 1f;
		gameMenu.GetComponent<menuImageSwapButton> ().gameMenuActivate ();

	}

	public void websiteStart() {

		if (Application.platform == RuntimePlatform.OSXWebPlayer
			|| Application.platform == RuntimePlatform.WindowsWebPlayer) {
			Application.ExternalCall ("OpenInNewTab", "http://www.medikidz.com");
		} else {
		
			Application.OpenURL("http://www.medikidz.com");
		}
		

	}

}

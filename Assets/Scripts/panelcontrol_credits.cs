﻿using UnityEngine;
using System.Collections;

public class panelcontrol_credits : MonoBehaviour {


	public GameObject Button1;
	public GameObject Button2;
	public GameObject Button3;
	public GameObject Button4;

	public GameObject Panel1;
	public GameObject Panel2;
	public GameObject Panel3;
	public GameObject Panel4;




	public void pressButton1(){
	
		Panel1.active = true;
		Panel2.active = false;
		Panel3.active = false;
		Panel4.active = false;
	}

	public void pressButton2(){
		
		Panel1.active = false;
		Panel2.active = true;
		Panel3.active = false;
		Panel4.active = false;
	}

	public void pressButton3(){
		
		Panel1.active = false;
		Panel2.active = false;
		Panel3.active = true;
		Panel4.active = false;
	}

	public void pressButton4(){
		
		Panel1.active = false;
		Panel2.active = false;
		Panel3.active = false;
		Panel4.active = true;
	}
}

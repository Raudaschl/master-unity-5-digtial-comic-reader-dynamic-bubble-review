//----------------------------------------------
//                 SpriteMask
//          Copyright © 2015 TrueSoft
//             support@truesoft.pl
//----------------------------------------------

using System;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Reflection;

[CustomEditor(typeof(SpriteMask))]
public class SpriteMaskEditor : Editor
{
	public override void OnInspectorGUI ()
	{
		SpriteMask mask = (SpriteMask)target;
		bool isPrefab = PrefabUtility.GetPrefabType (mask) == PrefabType.Prefab;
		if (isPrefab) {
			EditorGUILayout.HelpBox ("Prefab edit unavailable", MessageType.Info);
			return;
		}

		GUILayoutOption[] options = new GUILayoutOption[0];

		serializedObject.Update ();

		SpriteMask.Type currentType = mask.type;
		SpriteMask.Type newType = (SpriteMask.Type)EditorGUILayout.EnumPopup ("Type", currentType, options);
		if (currentType != newType) {
			mask.type = newType;
			mask.SendMessage ("RequestTypeApply", SendMessageOptions.DontRequireReceiver);
		}

		Vector2 currentSize = mask.size;
		Vector2 newSize = currentSize;

		Vector2 currentPivot = mask.pivot;
		Vector2 newPivot = currentPivot;
		
		Sprite currentSprite = mask.sprite;
		Sprite newSprite = currentSprite;
		
		Texture2D currentTexture = mask.texture;
		Texture2D newTexture = currentTexture;

		switch (newType) {
		case SpriteMask.Type.Sprite:
			newSprite = EditorGUILayout.ObjectField ("Sprite", currentSprite, typeof(Sprite), true, options) as Sprite;
			break;
		case SpriteMask.Type.Rectangle:
		case SpriteMask.Type.Texture:
			if (newType == SpriteMask.Type.Texture) {
				newTexture = EditorGUILayout.ObjectField ("Texture", currentTexture, typeof(Texture2D), true, options) as Texture2D;
			} 
			newSize = EditorGUILayout.Vector2Field ("Size", currentSize, options);
			newPivot = EditorGUILayout.Vector2Field ("Pivot", currentPivot, options);
			break;
		}

		if (GUI.changed) {
			if (currentSize != newSize) {
				mask.size = newSize;
			}

			if (currentPivot != newPivot) {
				mask.pivot = newPivot;
			}
			
			if (currentSprite != newSprite) {
				mask.sprite = newSprite;
			}
			
			if (currentTexture != newTexture) {
				mask.texture = newTexture;
			}
		}

		if (newType == SpriteMask.Type.Rectangle || newType == SpriteMask.Type.Texture) {
			EditorGUILayout.Space ();

			Renderer r = mask.GetComponent <Renderer> ();
			if (r != null) {
				string[] layerNames = GetSortingLayerNames ();
				int[] layerID = GetSortingLayerUniqueIDs ();

				int selected = -1;
				int sID = r.sortingLayerID;
				for (int i = 0; i < layerID.Length; i++) {
					if (sID == layerID [i]) {
						selected = i;
					}
				}
			
				if (selected == -1) {
					for (int i = 0; i < layerID.Length; i++) {
						if (layerID [i] == 0) {
							selected = i;
						}
					}
				}

				int newSelected = EditorGUILayout.Popup ("Sorting Layer", selected, layerNames);
				if (selected != newSelected) {
					r.sortingLayerID = layerID [newSelected];
				}

				r.sortingOrder = EditorGUILayout.IntField ("Order in Layer", r.sortingOrder, options);
			}
		}

		string msg = string.Concat ("Instance ID: ", mask.GetInstanceID (), 
		                            "\nStencil ID: ", mask.stencilId, 
		                            " (level=", mask.level, 
		                            ", id=" + mask.maskIdPerLevel,
		                            ")");
		EditorGUILayout.HelpBox (msg, MessageType.None);
	}

	public string[] GetSortingLayerNames ()
	{
		Type internalEditorUtilityType = typeof(InternalEditorUtility);
		PropertyInfo sortingLayersProperty = internalEditorUtilityType.GetProperty ("sortingLayerNames", BindingFlags.Static | BindingFlags.NonPublic);
		return (string[])sortingLayersProperty.GetValue (null, new object[0]);
	}
	
	public int[] GetSortingLayerUniqueIDs ()
	{
		Type internalEditorUtilityType = typeof(InternalEditorUtility);
		PropertyInfo sortingLayerUniqueIDsProperty = internalEditorUtilityType.GetProperty ("sortingLayerUniqueIDs", BindingFlags.Static | BindingFlags.NonPublic);
		return (int[])sortingLayerUniqueIDsProperty.GetValue (null, new object[0]);
	}
}
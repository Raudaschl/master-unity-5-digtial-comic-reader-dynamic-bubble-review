﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RosterCardCamera : MonoBehaviour {

	// Use this for initialization
	void Start()
	{
		Canvas canvas = gameObject.GetComponent<Canvas>();
		canvas.worldCamera = Camera.main;	
	}

}

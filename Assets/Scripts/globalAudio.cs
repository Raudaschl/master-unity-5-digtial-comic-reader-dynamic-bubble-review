﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class GloalAudioElements
{
	public AudioClip globalAudioClip;
	public bool loop = true;

	[Range(0.1f, 2.0f)]
	public float audioVolume = 1.0f;


	public bool fadeinAudio;
	[Range(0.1f, 1.0f)]
	public float audioFadeinTime = 0.5f;

	public bool fadeoutAudio;
	[Range(0.1f, 1.0f)]
	public float audioFadeoutTime = 0.5f;
}



public class globalAudio : MonoBehaviour {

	public GloalAudioElements[] globalAudioElement;


//	private GameObject globalBackgroundAudio1;
//	private AudioSource globalBackgroundAudio1AudioSource;
//	[Range(0.1f, 1.0f)]
//	 public float audio1FadeoutTime = 0.5f;
//
//	private GameObject globalBackgroundAudio2;
//	private AudioSource globalBackgroundAudio2AudioSource;
//	 [Range(0.1f, 1.0f)]
//	public float audio2FadeoutTime = 0.5f;
//
//	private GameObject globalBackgroundAudio3;
//	[Range(0.1f, 1.0f)]
//	public float audio3FadeoutTime = 0.5f;
//
//	private GameObject globalBackgroundAudio4;
//	[Range(0.1f, 1.0f)]
//	public float audio4FadeoutTime = 0.5f;
//
//	private GameObject globalBackgroundAudio5;
//	[Range(0.1f, 1.0f)]
//	public float audio5FadeoutTime = 0.5f;
//
//	private GameObject globalBackgroundAudio6;
//	[Range(0.1f, 1.0f)]
//	public float audio6FadeoutTime = 0.5f;
//
//	private GameObject globalBackgroundAudio7;
//	[Range(0.1f, 1.0f)]
//	public float audio7FadeoutTime = 0.5f;
//
//	private GameObject globalBackgroundAudio8;
//	[Range(0.1f, 1.0f)]
//	public float audio8FadeoutTime = 0.5f;
//
//	private GameObject globalBackgroundAudio9;
//	[Range(0.1f, 1.0f)]
//	public float audio9FadeoutTime = 0.5f;
//
//	private GameObject globalBackgroundAudio10;
//	[Range(0.1f, 1.0f)]
//	public float audio10FadeoutTime = 0.5f;


	
//	//Background1
//	float globalBackgroundAudio1_vol = 1.0f;
//	int globalBackgroundAudio1_fadeout_active = 0;
//	
//	//Background2
//	float globalBackgroundAudio2_vol = 1.0f;
//	int globalBackgroundAudio2_fadeout_active = 0;
//
//	//Background3
//	float globalBackgroundAudio3_vol = 1.0f;
//	int globalBackgroundAudio3_fadeout_active = 0;
//
//	//Background4
//	float globalBackgroundAudio4_vol = 1.0f;
//	int globalBackgroundAudio4_fadeout_active = 0;
//
//	//Background5
//	float globalBackgroundAudio5_vol = 1.0f;
//	int globalBackgroundAudio5_fadeout_active = 0;
//
//	//Background6
//	float globalBackgroundAudio6_vol = 1.0f;
//	int globalBackgroundAudio6_fadeout_active = 0;
//
//	//Background7
//	float globalBackgroundAudio7_vol = 1.0f;
//	int globalBackgroundAudio7_fadeout_active = 0;
//
//	//Background8
//	float globalBackgroundAudio8_vol = 1.0f;
//	int globalBackgroundAudio8_fadeout_active =0;
//
//		//Background9
//		float globalBackgroundAudio9_vol = 1.0f;
//	int globalBackgroundAudio9_fadeout_active =0;
//
//	//Background10
//	float globalBackgroundAudio10_vol = 1.0f;
//	int globalBackgroundAudio10_fadeout_active =0;

	private AudioSource[] audioSources;

	private int Direction;
	

	void Awake(){

		for (int i = 0; i < globalAudioElement.Length; i++){

			if (globalAudioElement[i].globalAudioClip != null){
			AudioSource audioSource = gameObject.AddComponent<AudioSource>();

			audioSource.playOnAwake = false;
			audioSource.loop = globalAudioElement[i].loop;
			audioSource.clip = globalAudioElement[i].globalAudioClip;
			}

		}

		audioSources = gameObject.GetComponents<AudioSource> ();

	}

	// Use this for initialization
	void Start () {


//
//		globalBackgroundAudio1 = GameObject.Find("globalBackgroundAudio1");
//		globalBackgroundAudio1AudioSource = globalBackgroundAudio1.GetComponent<AudioSource> ();
//
//		globalBackgroundAudio2 = GameObject.Find("globalBackgroundAudio2");
//		globalBackgroundAudio2AudioSource = globalBackgroundAudio2.GetComponent<AudioSource> ();
//
//		globalBackgroundAudio3 = GameObject.Find("globalBackgroundAudio3");
//		globalBackgroundAudio4 = GameObject.Find("globalBackgroundAudio4");
//		globalBackgroundAudio5 = GameObject.Find("globalBackgroundAudio5");
//		globalBackgroundAudio6 = GameObject.Find("globalBackgroundAudio6");
//		globalBackgroundAudio7 = GameObject.Find("globalBackgroundAudio7");
//		globalBackgroundAudio8 = GameObject.Find("globalBackgroundAudio8");
//		globalBackgroundAudio9 = GameObject.Find("globalBackgroundAudio9");
//		globalBackgroundAudio10 = GameObject.Find("globalBackgroundAudio10");

	
	}

	public int getDirection(){
		//Find loader object


		return loading_prefab.Direction;


	}
	
//	// Update is called once per frame
//	void Update () {
//
//
//
//
//		globalBackgroundAudio1AudioSource.volume = globalBackgroundAudio1_vol;
//		if (globalBackgroundAudio1AudioSource.volume > 0 && globalBackgroundAudio1_fadeout_active == 1) {
//			globalBackgroundAudio1_vol_fadeout ();
//		} 
//		if (globalBackgroundAudio1AudioSource.volume <= 0) {
//			globalBackgroundAudio1_fadeout_active = 0;
//			globalBackgroundAudio1AudioSource.Stop();
//		}
//		
//		//Handle background2 fade out
//		globalBackgroundAudio2AudioSource.volume = globalBackgroundAudio2_vol;
//		if (globalBackgroundAudio2AudioSource.volume > 0 && globalBackgroundAudio2_fadeout_active == 1) {
//			globalBackgroundAudio2_vol_fadeout ();
//		} 
//		if (globalBackgroundAudio2AudioSource.volume <= 0) {
//			globalBackgroundAudio2_fadeout_active = 0;
//			globalBackgroundAudio2AudioSource.Stop();
//		}
//
//		//Handle background3 fade out
//		globalBackgroundAudio3.GetComponent<AudioSource>().volume = globalBackgroundAudio3_vol;
//		if (globalBackgroundAudio3.GetComponent<AudioSource>().volume > 0 && globalBackgroundAudio3_fadeout_active == 1) {
//			globalBackgroundAudio3_vol_fadeout ();
//		} 
//		if (globalBackgroundAudio3.GetComponent<AudioSource>().volume <= 0) {
//			globalBackgroundAudio3_fadeout_active = 0;
//			globalBackgroundAudio3.GetComponent<AudioSource>().Stop();
//		}
//
//		//Handle background4 fade out
//		globalBackgroundAudio4.GetComponent<AudioSource>().volume = globalBackgroundAudio4_vol;
//		if (globalBackgroundAudio4.GetComponent<AudioSource>().volume > 0 && globalBackgroundAudio4_fadeout_active == 1) {
//			globalBackgroundAudio4_vol_fadeout ();
//		} 
//		if (globalBackgroundAudio4.GetComponent<AudioSource>().volume <= 0) {
//			globalBackgroundAudio4_fadeout_active = 0;
//			globalBackgroundAudio4.GetComponent<AudioSource>().Stop();
//		}
//
//		//Handle background5 fade out
//		globalBackgroundAudio5.GetComponent<AudioSource>().volume = globalBackgroundAudio5_vol;
//		if (globalBackgroundAudio5.GetComponent<AudioSource>().volume > 0 && globalBackgroundAudio5_fadeout_active == 1) {
//			globalBackgroundAudio5_vol_fadeout ();
//		} 
//		if (globalBackgroundAudio5.GetComponent<AudioSource>().volume <= 0) {
//			globalBackgroundAudio5_fadeout_active = 0;
//			globalBackgroundAudio5.GetComponent<AudioSource>().Stop();
//		}
//
//		//Handle background6 fade out
//		globalBackgroundAudio6.GetComponent<AudioSource>().volume = globalBackgroundAudio6_vol;
//		if (globalBackgroundAudio6.GetComponent<AudioSource>().volume > 0 && globalBackgroundAudio6_fadeout_active == 1) {
//			globalBackgroundAudio6_vol_fadeout ();
//		} 
//		if (globalBackgroundAudio6.GetComponent<AudioSource>().volume <= 0) {
//			globalBackgroundAudio6_fadeout_active = 0;
//			globalBackgroundAudio6.GetComponent<AudioSource>().Stop();
//		}
//
//		//Handle background7 fade out
//		globalBackgroundAudio7.GetComponent<AudioSource>().volume = globalBackgroundAudio7_vol;
//		if (globalBackgroundAudio7.GetComponent<AudioSource>().volume > 0 && globalBackgroundAudio7_fadeout_active == 1) {
//			globalBackgroundAudio7_vol_fadeout ();
//		} 
//		if (globalBackgroundAudio7.GetComponent<AudioSource>().volume <= 0) {
//			globalBackgroundAudio7_fadeout_active = 0;
//			globalBackgroundAudio7.GetComponent<AudioSource>().Stop();
//		}
//
//		//Handle background8 fade out
//		globalBackgroundAudio8.GetComponent<AudioSource>().volume = globalBackgroundAudio8_vol;
//		if (globalBackgroundAudio8.GetComponent<AudioSource>().volume > 0 && globalBackgroundAudio8_fadeout_active == 1) {
//			globalBackgroundAudio8_vol_fadeout ();
//		} 
//		if (globalBackgroundAudio8.GetComponent<AudioSource>().volume <= 0) {
//			globalBackgroundAudio8_fadeout_active = 0;
//			globalBackgroundAudio8.GetComponent<AudioSource>().Stop();
//		}
//
//		//Handle background9 fade out
//		globalBackgroundAudio9.GetComponent<AudioSource>().volume = globalBackgroundAudio9_vol;
//		if (globalBackgroundAudio9.GetComponent<AudioSource>().volume > 0 && globalBackgroundAudio9_fadeout_active == 1) {
//			globalBackgroundAudio9_vol_fadeout ();
//		} 
//		if (globalBackgroundAudio9.GetComponent<AudioSource>().volume <= 0) {
//			globalBackgroundAudio9_fadeout_active = 0;
//			globalBackgroundAudio9.GetComponent<AudioSource>().Stop();
//		}
//
//		//Handle background10 fade out
//		globalBackgroundAudio10.GetComponent<AudioSource>().volume = globalBackgroundAudio10_vol;
//		if (globalBackgroundAudio10.GetComponent<AudioSource>().volume > 0 && globalBackgroundAudio10_fadeout_active == 1) {
//			globalBackgroundAudio10_vol_fadeout ();
//		} 
//		if (globalBackgroundAudio10.GetComponent<AudioSource>().volume <= 0) {
//			globalBackgroundAudio10_fadeout_active = 0;
//			globalBackgroundAudio10.GetComponent<AudioSource>().Stop();
//		}
//	
//	}


	//GLOBAL Music Effects
	
	public void globalBackgroundAudio_audio(int globalAudioValueStart) {



		if (globalAudioValueStart > audioSources.Length) {
			AudioSource currentAudioStart = audioSources [globalAudioValueStart];
		
			if (getDirection () == 2) {
				currentAudioStart.volume = 1.0f;

				currentAudioStart.Play ();

			} else {
				StartCoroutine (globalBackgroundAudio_fadeout (globalAudioValueStart));
			
			}
		
		}
		
	}

	//GLOBAL Music FADEOUT
	
	public IEnumerator globalBackgroundAudio_fadeout(int globalAudioValue) {

		float interval = 0.1f;

		if (globalAudioValue > audioSources.Length) {

		AudioSource currentAudio = audioSources [globalAudioValue];

		while (currentAudio.volume > 0) {
			currentAudio.volume = currentAudio.volume - (0.1f);


			if (currentAudio.volume <= 0){
				currentAudio.Stop();
				
			}

			yield return new WaitForSeconds(interval);

//			Debug.Log (currentAudio.volume);
		}
		}

	}
	

//
//
//
//	//Background1 Music Effects
//	
//	public void globalBackgroundAudio1_audio() {
//
//		if (getDirection() == 2) {
//			globalBackgroundAudio1_vol = 1.0f;
//
//			globalBackgroundAudio1AudioSource.Play ();
////			Debug.Log ("playing");
//		} else {
//			globalBackgroundAudio1_fadeout();
//		
//		}
//
//	}
//
//	public void globalBackgroundAudio1_fadeout(){
//		globalBackgroundAudio1_fadeout_active = 1;
//
//	}
//
//
//	//Background2 Music Effects
//
//	public void globalBackgroundAudio2_audio() {
//		if (getDirection() == 2) {
//			globalBackgroundAudio2AudioSource.Play ();
//			globalBackgroundAudio2_vol = 1.0f;
//		} else {
//			globalBackgroundAudio2_fadeout();
//			
//		}
//		
//	}
//	
//	//Background Fade out function
//	public void globalBackgroundAudio2_fadeout(){
//		globalBackgroundAudio2_fadeout_active = 1;
//
//	}
//
//	//Background3 Music Effects
//	
//	public void globalBackgroundAudio3_audio() {
//		if (getDirection() == 2) {
//			globalBackgroundAudio3.GetComponent<AudioSource> ().Play ();
//			globalBackgroundAudio3_vol = 1.0f;
//		} else {
//			globalBackgroundAudio3_fadeout();
//			
//		}
//		
//	}
//	
//	//Background Fade out function
//
//
//	public void globalBackgroundAudio3_fadeout(){
//		globalBackgroundAudio3_fadeout_active = 1;
//
//	}
//
//	//Background4 Music Effects
//	
//	public void globalBackgroundAudio4_audio() {
//		if (getDirection() == 2) {
//			globalBackgroundAudio4.GetComponent<AudioSource> ().Play ();
//		} else {
//			globalBackgroundAudio4_fadeout();
//			
//		}
//		
//	}
//	
//	//Background Fade out function
//	public void globalBackgroundAudio4_fadeout(){
//		globalBackgroundAudio4_fadeout_active = 1;
//
//	}
//
//	//Background5 Music Effects
//	
//	public void globalBackgroundAudio5_audio() {
//		if (getDirection() == 2) {
//			globalBackgroundAudio5.GetComponent<AudioSource> ().Play ();
//		} else {
//			globalBackgroundAudio5_fadeout();
//			
//		}
//		
//	}
//	
//	//Background Fade out function
//	public void globalBackgroundAudio5_fadeout(){
//		globalBackgroundAudio5_fadeout_active = 1;
//
//	} 
//
//	//Background6 Music Effects
//	
//	public void globalBackgroundAudio6_audio() {
//		if (getDirection() == 2) {
//			globalBackgroundAudio6.GetComponent<AudioSource> ().Play ();
//		} else {
//			globalBackgroundAudio6_fadeout();
//			
//		}
//		
//	}
//	
//	//Background Fade out function
//	public void globalBackgroundAudio6_fadeout(){
//		globalBackgroundAudio6_fadeout_active = 1;
//
//	}
//
//	//Background7 Music Effects
//	
//	public void globalBackgroundAudio7_audio() {
//		if (getDirection() == 2) {
//			globalBackgroundAudio7_vol = 1.0f;
//			globalBackgroundAudio7.GetComponent<AudioSource> ().Play ();
//		} else {
//			globalBackgroundAudio7_fadeout();
//			
//		}
//		
//	}
//	
//	//Background Fade out function
//	public void globalBackgroundAudio7_fadeout(){
//
//		globalBackgroundAudio7_fadeout_active = 1;
//
//	}
//
//	//Background8 Music Effects
//	
//	public void globalBackgroundAudio8_audio() {
//		if (getDirection() == 2) {
//			globalBackgroundAudio8_vol = 1.0f;
//			globalBackgroundAudio8.GetComponent<AudioSource> ().Play ();
//		} else {
//			globalBackgroundAudio8_fadeout();
//			
//		}
//		
//	}
//	
//	//Background Fade out function
//	public void globalBackgroundAudio8_fadeout(){
//
//		globalBackgroundAudio8_fadeout_active = 1;
//
//	}
//
//
//	//Background9 Music Effects
//	
//	public void globalBackgroundAudio9_audio() {
//		if (getDirection() == 2) {
//			globalBackgroundAudio9_vol = 1.0f;
//			globalBackgroundAudio9.GetComponent<AudioSource> ().Play ();
//		} else {
//			globalBackgroundAudio9_fadeout();
//			
//		}
//		
//	}
//	
//	//Background Fade out function
//	public void globalBackgroundAudio9_fadeout(){
//		globalBackgroundAudio9_fadeout_active = 1;
//
//	}
//
//
//	//Background9 Music Effects
//	
//	public void globalBackgroundAudio10_audio() {
//		if (getDirection() == 2) {
//			globalBackgroundAudio10_vol = 1.0f;
//			globalBackgroundAudio10.GetComponent<AudioSource> ().Play ();
//		} else {
//			globalBackgroundAudio10_fadeout();
//			
//		}
//		
//	}
//	
//	//Background Fade out function
//	public void globalBackgroundAudio10_fadeout(){
//		globalBackgroundAudio10_fadeout_active = 1;
//
//	}

	//Fadeout all

	public void fadeOutAllGlobalAudio(){

//		globalBackgroundAudio1AudioSource.Stop();
//		globalBackgroundAudio2AudioSource.Stop();
//		globalBackgroundAudio3.GetComponent<AudioSource>().Stop();
//		globalBackgroundAudio4.GetComponent<AudioSource>().Stop();
//		globalBackgroundAudio5.GetComponent<AudioSource>().Stop();
//		globalBackgroundAudio6.GetComponent<AudioSource>().Stop();
//		globalBackgroundAudio7.GetComponent<AudioSource>().Stop();
//		globalBackgroundAudio8.GetComponent<AudioSource>().Stop();
//		globalBackgroundAudio9.GetComponent<AudioSource>().Stop();
//		globalBackgroundAudio10.GetComponent<AudioSource>().Stop();
//
//		globalBackgroundAudio1_vol = 1.0f;
//		globalBackgroundAudio2_vol = 1.0f;
//		globalBackgroundAudio3_vol = 1.0f;
//		globalBackgroundAudio4_vol = 1.0f;
//		globalBackgroundAudio5_vol = 1.0f;
//		globalBackgroundAudio6_vol = 1.0f;
//		globalBackgroundAudio7_vol = 1.0f;
//		globalBackgroundAudio8_vol = 1.0f;
//		globalBackgroundAudio9_vol = 1.0f;
//		globalBackgroundAudio10_vol = 1.0f;
//		
	

		
		//		globalBackgroundAudio1_fadeout();
//		globalBackgroundAudio2_fadeout();
//		globalBackgroundAudio3_fadeout();
//		globalBackgroundAudio4_fadeout();
//		globalBackgroundAudio5_fadeout();
//		globalBackgroundAudio6_fadeout();
//		globalBackgroundAudio7_fadeout();
//		globalBackgroundAudio8_fadeout();
//		globalBackgroundAudio9_fadeout();
//		globalBackgroundAudio10_fadeout();


	}


	
//	//Audio fadeout function
//	
//	private void globalBackgroundAudio1_vol_fadeout(){
//		globalBackgroundAudio1_vol = globalBackgroundAudio1_vol - (audio1FadeoutTime*Time.deltaTime);
//	}
//	
//	private void globalBackgroundAudio2_vol_fadeout(){
//		globalBackgroundAudio2_vol = globalBackgroundAudio2_vol - (audio2FadeoutTime*Time.deltaTime);
//	}
//
//	private void globalBackgroundAudio3_vol_fadeout(){
//		globalBackgroundAudio3_vol = globalBackgroundAudio3_vol - (audio3FadeoutTime*Time.deltaTime);
//	}
//
//	private void globalBackgroundAudio4_vol_fadeout(){
//		globalBackgroundAudio4_vol = globalBackgroundAudio4_vol - (audio4FadeoutTime*Time.deltaTime);
//	}
//
//	private void globalBackgroundAudio5_vol_fadeout(){
//		globalBackgroundAudio5_vol = globalBackgroundAudio5_vol - (audio5FadeoutTime*Time.deltaTime);
//	}
//
//	private void globalBackgroundAudio6_vol_fadeout(){
//		globalBackgroundAudio6_vol = globalBackgroundAudio6_vol - (audio6FadeoutTime*Time.deltaTime);
//	}
//
//	private void globalBackgroundAudio7_vol_fadeout(){
//		globalBackgroundAudio7_vol = globalBackgroundAudio7_vol - (audio7FadeoutTime*Time.deltaTime);
//	}
//
//	private void globalBackgroundAudio8_vol_fadeout(){
//		globalBackgroundAudio8_vol = globalBackgroundAudio8_vol - (audio8FadeoutTime*Time.deltaTime);
//	}
//
//	private void globalBackgroundAudio9_vol_fadeout(){
//		globalBackgroundAudio9_vol = globalBackgroundAudio9_vol - (audio9FadeoutTime*Time.deltaTime);
//	}
//
//	private void globalBackgroundAudio10_vol_fadeout(){
//		globalBackgroundAudio10_vol = globalBackgroundAudio10_vol - (audio9FadeoutTime*Time.deltaTime);
//	}

}
﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class webplayerCheckDestroy : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if( Application.platform == RuntimePlatform.OSXWebPlayer
		   || Application.platform == RuntimePlatform.WindowsWebPlayer )
		{
			GetComponent<Button>().interactable = false;
		}
	}

}
